<?php
/**
 * @package Custom Masonry Sections
 * @subpackage Account Page
 * @version 0.1
 */

define('CMSECT_CUST_ACCOUNT_PASS_MIN_CHAR', 5);

add_shortcode('cmsect_account_page', 'cmsect_cust_account_page_shortcode');
function cmsect_cust_account_page_shortcode($atts) {
	
	if(!is_user_logged_in()) {
		global $cmsect_custom_modules;
		if(in_array('login-for-restricted', $cmsect_custom_modules)
		&& DOING_AJAX !== true) {
			return '<script type="text/javascript">
			jQuery(document).ready(function() {
				jQuery(".cmsect_cust_popup_overlay").fadeIn("fast", function() {
					jQuery("#cmsect_cust_lfr_login").fadeIn("fast");
				});
			});
			</script>';
		}
		return '';
	}
	
	$atts = shortcode_atts(array(
		
	), $atts, 'cmsect');
	
	extract($atts);
	
	global $current_user;
	get_currentuserinfo();
	
	$account = cmsect_cust_account_get_info($current_user->ID);
	
	echo apply_filters('cmsect_cust_account_page_title', '');
	?><form class="cmsect-cust-account clearfix" method="post" action="">
		<input type="hidden" name="cmsect_account_page_action" value="save" />
	<?php if(isset($_SESSION['CMSECT_CUST_ACCOUNT_SAVE_ERROR'])) { ?>
		<p class="message"><?php echo $_SESSION['CMSECT_CUST_ACCOUNT_SAVE_ERROR']; ?></p>
	<?php unset($_SESSION['CMSECT_CUST_ACCOUNT_SAVE_ERROR']); } ?>
	<?php if(isset($_SESSION['CMSECT_CUST_ACCOUNT_SAVE_SUCCESS'])) { ?>
		<p class="message success"><?php echo $_SESSION['CMSECT_CUST_ACCOUNT_SAVE_SUCCESS']; ?></p>
	<?php unset($_SESSION['CMSECT_CUST_ACCOUNT_SAVE_SUCCESS']); } ?>
		<fieldset>
			<h3>Personal Information</h3>
			<label class="one-half">
				First Name
				<input type="text" name="account[first_name]" value="<?php echo $account['first_name']; ?>" />
			</label>
			<label class="one-half last">
				Last Name
				<input type="text" name="account[last_name]" value="<?php echo $account['last_name']; ?>" />
			</label>
			<div class="clear"></div>
			<label class="one-half">
				Email
				<input type="text" readonly="" disabled="" value="<?php echo $account['email']; ?>" />
			</label>
			<label class="one-half last">
				Contact Number
				<input type="text" name="account[contact_number]" value="<?php echo $account['contact_number']; ?>" />
			</label>
			<div class="clear"></div>
			<label class="one-half last has-field-address">
				Address
				<textarea name="account[address]"><?php echo $account['address']; ?></textarea>
			</label>
			<div class="clear"></div>
			<label class="has-field-description">
				About Yourself
				<textarea name="account[description]"><?php echo $account['description']; ?></textarea>
			</label>
			<div class="clear"></div><?php
			if(defined('CMSECT_CUST_ACCOUNT_FIELDS_CHANGE_PASS_ON_LEFT')) { ?>
			<p>&nbsp;</p>
			<div class="cmsect-cust-account-change-password">
				<h3>Account Password</h3>
				<p>Would you like to <a href="javascript: void(0)" class="cmsect_cust_account_show_change_pass">change you password</a>?</p>
				<label class="one-half" style="display: none">
					New Password
					<input type="password" name="account[new_password]" autocomplete="off" value="-" />
				</label>
				<label class="one-half" style="display: none">
					Retype New Password
					<input type="password" name="account[confirm_new_password]" autocomplete="off" value="-" />
				</label>
			</div>
			<?php } ?>
		</fieldset>
		<fieldset>
			<?php do_action('cmsect_cust_account_extra_fields');
			if(!defined('CMSECT_CUST_ACCOUNT_FIELDS_CHANGE_PASS_ON_LEFT')) { ?>
			<h3>Account Password</h3>
			<p>Would you like to <a href="javascript: void(0)" class="cmsect_cust_account_show_change_pass">change you password</a>?</p>
			<label class="one-half" style="display: none">
				New Password
				<input type="password" name="account[new_password]" autocomplete="off" value="-" />
			</label>
			<label class="one-half" style="display: none">
				Retype New Password
				<input type="password" name="account[confirm_new_password]" autocomplete="off" value="-" />
			</label>
			<?php } ?>
		</fieldset>
		<div class="clear"></div>
		<button type="submit"><?php echo apply_filters('cmsect_cust_account_submit_button_text', 'Save Changes'); ?></button>
	</form><?php
}

add_action('wp_print_scripts', 'cmsect_cust_account_wp_print_scripts', 1000);
function cmsect_cust_account_wp_print_scripts() {
	global $post;
	if(has_shortcode($post->post_content, 'cmsect_account_page') || 1) { // allow css to display everywhere ?>
<style>
form.cmsect-cust-account,
form.cmsect-cust-account * {
	   -moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
	     -o-box-sizing: border-box;
	    -ms-box-sizing: border-box;
	        box-sizing: border-box;
}
form.cmsect-cust-account { padding: 0 0 50px; max-width: 100%; width: 100% }
form.cmsect-cust-account p { font-size: 13px }
form.cmsect-cust-account .clear { clear: both }
form.cmsect-cust-account .message { background-color: #fcb; border-radius: 3px; color: rgba(0, 0, 0, 0.4); margin: 0 0 20px; padding: 5px 20px; }
form.cmsect-cust-account .message.success { background-color: #8ed }
form.cmsect-cust-account fieldset { border-left: 5px solid #9fffef; float: left; margin-right: 2%; padding-left: 2%; width: 48%; }
form.cmsect-cust-account fieldset + fieldset { margin-left: 2%; margin-right: 0; }
form.cmsect-cust-account label { display: block; float: left; font-size: 13px; padding: 10px 0; width: 100% }
form.cmsect-cust-account label.one-half { width: 48%; margin-right: 2% }
form.cmsect-cust-account label.one-half + label.one-half { margin-left: 2%; margin-right: 0 }
form.cmsect-cust-account label input,
form.cmsect-cust-account label select,
form.cmsect-cust-account label textarea {
	border: #ccc 1px solid;
	border-radius: 3px;
	font-family: inherit;
	font-size: inherit;
	padding: 10px 15px;
	width: 100%
}
form.cmsect-form [type="submit"],
form.cmsect-cust-account [type="submit"] {
	background-color: #1abc9c;
	border: 0 none;
	border-radius: 3px;
	color: #fff;
	font-family: inherit;
	font-weight: bold;
	margin: 20px 0;
	padding: 7px 15px;
	text-transform: uppercase;
}
form.cmsect-form [type="submit"][disabled],
form.cmsect-form [type="submit"].disabled,
form.cmsect-cust-account [type="submit"][disabled] { opacity: 0.3 }
form.cmsect-form [type="submit"]:hover,
form.cmsect-cust-account [type="submit"]:hover { background-color: #2accac }
</style><?php
}

add_action('wp_footer', 'cmsect_cust_account_wp_footer', 1000);
function cmsect_cust_account_wp_footer() { ?>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery('form.cmsect-cust-account').submit(function() {
			jQuery(this).find('[type="submit"]').attr('disabled', true);
		});
		jQuery('.cmsect_cust_account_show_change_pass').click(function(e) {
			e.preventDefault();
			var self = jQuery(this);
			jQuery('input[name^="account"][name*="password"]').val('');
			jQuery(self).closest('p').slideUp('medium', function() {
				jQuery(self).closest('p').siblings('label').slideDown('medium', function() {
					jQuery('input[name="account[new_password]"]').select();
				});
			});
		});
	});
</script>
	<?php }
}

add_action('template_redirect', 'cmsect_cust_account_template_redirect');
function cmsect_cust_account_template_redirect() {
	if(is_user_logged_in()
	&& $_POST['cmsect_account_page_action'] == 'save') {
		$posted_account = $_POST['account'];
		if(!isset($error) && trim($posted_account['first_name']) == '') {
			$error = 'Please enter your first name.';
		}
		if(!isset($error) && trim($posted_account['last_name']) == '') {
			$error = 'Please enter your last name.';
		}
		if(!isset($error) && trim($posted_account['contact_number']) == '') {
			$error = 'Please enter your contact number.';
		}/*
		if(!isset($error) && trim($posted_account['address']) == '') {
			$error = 'Please enter your postal address.';
		}*/
		if(!isset($error) && trim($posted_account['new_password']) == '') {
			$error = 'Please enter a new password.';
		}
		if(!isset($error)
		&& trim($posted_account['new_password']) != ''
		&& trim($posted_account['new_password']) != '-'
		&& strlen(trim($posted_account['new_password'])) < CMSECT_CUST_ACCOUNT_PASS_MIN_CHAR) {
			$error = 'The entered password is too weak. Please enter a strong password with atleast '.CMSECT_CUST_ACCOUNT_PASS_MIN_CHAR.' characters.';
		}
		if(!isset($error) && trim($posted_account['confirm_new_password']) == '') {
			$error = 'Please retype the new password.';
		}
		if(!isset($error)
		&& trim($posted_account['confirm_new_password']) != ''
		&& trim($posted_account['confirm_new_password']) != '-'
		&& trim($posted_account['confirm_new_password']) != trim($posted_account['new_password'])) {
			$error = 'The entered passwords do not match. Please check your entry and retry.';
		}
		
		if(isset($error)) {
			$_SESSION['CMSECT_CUST_ACCOUNT_SAVE_ERROR'] = $error;
			$_SESSION['CMSECT_CUST_ACCOUNT_SAVE_POSTDATA'] = $posted_account;
		} else {
			global $current_user;
			get_currentuserinfo();
			
			update_user_meta($current_user->ID, 'first_name', $posted_account['first_name']);
			update_user_meta($current_user->ID, 'last_name', $posted_account['last_name']);
			update_user_meta($current_user->ID, 'contact_number', $posted_account['contact_number']);
			update_user_meta($current_user->ID, 'address', $posted_account['address']);
			update_user_meta($current_user->ID, 'description', $posted_account['description']);
			
			if(isset($posted_account['extra_fields']))
				update_user_meta($current_user->ID, '_cmsect_cust_account_extra_fields', $posted_account['extra_fields']);
			
			$_SESSION['CMSECT_CUST_ACCOUNT_SAVE_SUCCESS'] = 'Your information has been saved';
			
			if(trim($posted_account['new_password']) != '-'
			&& strlen(trim($posted_account['new_password'])) >= CMSECT_CUST_ACCOUNT_PASS_MIN_CHAR
			&& trim($posted_account['new_password']) == trim($posted_account['confirm_new_password'])) {
				$password_changed = wp_update_user(array(
					'ID' => $current_user->ID,
					'user_pass' => $posted_account['new_password']
				));
				if(is_wp_error($password_changed)) {
					$_SESSION['CMSECT_CUST_ACCOUNT_SAVE_ERROR'] = 'Your password could not be changed. '.$password_changed->get_error_message();
				} else {
					$_SESSION['CMSECT_CUST_ACCOUNT_SAVE_SUCCESS'] = (isset($_SESSION['CMSECT_CUST_ACCOUNT_SAVE_SUCCESS']) ? $_SESSION['CMSECT_CUST_ACCOUNT_SAVE_SUCCESS'].'. ' : '')
						.'Your password has been changed successfully.';
				}
			}
		}
		wp_redirect('http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
		exit();
		
	}
}

function cmsect_cust_account_get_info($user_id) {
	$account = array();
	
	$user = get_user_by('id', $user_id);
	if(!$user) return $account;
	
	$account['first_name'] = get_user_meta($user_id, 'first_name', true);
	$account['last_name'] = get_user_meta($user_id, 'last_name', true);
	
	$account['email'] = $user->data->user_email;
	
	$account['contact_number'] = get_user_meta($user_id, 'contact_number', true);
	$account['address'] = get_user_meta($user_id, 'address', true);
	$account['description'] = get_user_meta($user_id, 'description', true);
	
	return $account;
}

?>