<?php
/**
 * @package Custom Masonry Sections
 * @subpackage Likes
 * @version 0.1
 */

add_action('admin_menu', 'cmsect_cust_likes_admin_menu');
function cmsect_cust_likes_admin_menu() {
	add_menu_page('Users Wishlist', 'Users Wishlist', 'manage_options', 'cmsect_users_wishlist', 'cmsect_users_wishlist_page_content', 'dashicons-heart');
}
function cmsect_users_wishlist_page_content() { ?>
<div class="wrap">
	<h2>
		Users' Wishlist
	</h2>
	<span class="">Please select a user below to view their wishlist.</span>
	<p></p>
	
	<form class="frm_cmsect_cust_likes_wishlist" method="post" action="">
		<label class="has-cmsect-user" style="display: inline-block; width: 200px; margin-right: 50px">
			User<br />
			<select name="cmsect_user">
				<option value="">Select...</option>
		<?php
		$args = array(
			'blog_id'      => $GLOBALS['blog_id'],
			'role'         => '',
			'meta_key'     => '',
			'meta_value'   => '',
			'meta_compare' => '',
			'meta_query'   => array(),
			'date_query'   => array(),        
			'include'      => array(),
			'exclude'      => array(),
			'orderby'      => 'login',
			'order'        => 'ASC',
			'offset'       => '',
			'search'       => '',
			'number'       => '',
			'count_total'  => false,
			'fields'       => 'all',
			'who'          => ''
		);
		$users = get_users($args);
		if(count($users) > 0) {
			foreach($users as $user) { ?>
				<option value="<?php echo $user->ID; ?>"<?php echo $_POST['cmsect_user'] == $user->ID ? ' selected=""' : ''; ?>><?php echo $user->data->display_name.' ('.$user->data->user_email.')'; ?></option><?php
			}
		}
		?>
			</select>
		</label>
		<?php $default_post_type = $_POST['cmsect_post_type'] ? $_POST['cmsect_post_type'] : CMSECT_CUST_DEFAULT_ITEMS_TYPE; ?>
		<label class="has-cmsect-post-type" style="display: inline-block">
			Post Type<br />
			<select name="cmsect_post_type">
				<option value="post"<?php echo $default_post_type == 'post' ? ' selected=""' : ''; ?>>WP Posts</option>
		<?php
		$args = array(
			'public'                => true,
			'exclude_from_search'   => false,
			'_builtin'              => false
		);
		$post_types = get_post_types($args, 'objects', 'and');
		if(count($post_types) > 0) {
			foreach($post_types as $post_type_slug => $post_type_options) { ?>
				<option value="<?php echo $post_type_slug; ?>"<?php echo $default_post_type == $post_type_slug ? ' selected=""' : ''; ?>><?php echo $post_type_options->labels->name; ?></option><?php
			}
		}
		?>
			</select>
		</label>
		<p><input type="submit" class="button-primary" value="Show" /></p>
	</form>
	<script type="text/javascript">
	jQuery(document).ready(function() { jQuery('.frm_cmsect_cust_likes_wishlist select').change(function() { jQuery(this).closest('form').submit(); }); });
	</script>
	
	<p>&nbsp;</p>
	<?php if(isset($_POST['cmsect_user']) && $_POST['cmsect_user'] > 0) {
	?><link rel="stylesheet" href="<?php echo plugin_dir_url(__FILE__).'../css/cmsect-style.php'; ?>" /><?php
		echo cmsect_cust_my_likes_shortcode(array(
			'user_id' => $_POST['cmsect_user'],
			'post_type' => $_POST['cmsect_post_type']
		));
		cmsect_wp_footer();
		cmsect_cust_likes_wp_footer();
	} ?>
	
</div><?php
}

global $cmsect_custom_modules_global_admin_fields;
$cmsect_custom_modules_global_admin_fields['likes'] = 'cmsect_cust_likes_global_admin_fields';

function cmsect_cust_likes_global_admin_fields() {
	global $cms_section;
	?><div class="field">
		<label class="title">ADD-ON: "Likes"</label>
		<span class="descr mrgn-b-10">The "Likes" addon allows logged in users to save items to a list.</span>
		
		<div class="field-option mrgn-b-10 hidden" style="display: block">
			<div class="sub-option mrgn-l-20" style="display: block">
				<label>Please use the shortcode <code>[cmsect_cust_my_likes]</code> to display the logged in user's liked items.</label>
			</div>
		</div>
		
		<div class="field-option mrgn-b-10 hidden" style="display: block">
			<div class="sub-option mrgn-l-20" style="display: block">
				<label>To add custom CSS to the list of liked items, please prefix the CSS definitions with <code>#cmsect-my-likes.cmsect ...</code></label>
				<span class="descr">
					<strong>For example:</strong><br />
					<code>#cmsect-my-likes.cmsect .cmsect-item { width: 150px }</code>
				</span>
			</div>
		</div>
	</div><?php
}

add_action('wp_enqueue_scripts', function() {
	global $cmsect_shortcodes_list;
	array_push($cmsect_shortcodes_list, 'cmsect_cust_my_likes');
}, 500);

add_shortcode('cmsect_cust_my_likes', 'cmsect_cust_my_likes_shortcode');
function cmsect_cust_my_likes_shortcode($atts) {
	
	if(!is_user_logged_in()) {
		global $cmsect_custom_modules;
		if(in_array('login-for-restricted', $cmsect_custom_modules)
		&& DOING_AJAX !== true) {
			return '<script type="text/javascript">
			jQuery(document).ready(function() {
				jQuery(".cmsect_cust_popup_overlay").fadeIn("fast", function() {
					jQuery("#cmsect_cust_lfr_login").fadeIn("fast");
				});
			});
			</script>';
		}
		return '';
	}
	
	$atts = shortcode_atts(array(
		'user_id' => '0',
		'post_type' => 'post',
		'item_data_thumbnail_flip_cf' => '',
		'item_data_shortdescr_length' => 20
	), $atts, 'cmsect');
	
	extract($atts);
	
	if($user = get_user_by('id', $user_id)) {}
	else {
		global $current_user;
		get_currentuserinfo();
		$user = $current_user;
	}
	
	$liked_items = cmsect_cust_likes_get_liked_items($user->ID);
	$cmsect_args = array(
		'posts_per_page'   => -1,
		'offset'           => 0,
		'category'         => '',/*
		'category_name'    => '',*/
		'orderby'          => 'post__in',
		'order'            => 'ASC',
		'include'          => implode(',', $liked_items),/*
		'exclude'          => '',/*
		'meta_key'         => '',
		'meta_value'       => '',*/
		'post_type'        => $post_type,/*
		'post_mime_type'   => '',
		'post_parent'      => '',
		'author'           => '',*/
		'post_status'      => 'publish'
	);
	
	$output_HTML = '<!-- START: Custom Masonry Section -->'
	.apply_filters('cmsect_cust_likes_my_likes_page_title', '')
	.'<div class="cmsect-clearfix"></div>'
	.'<div class="cmsect-container">'
	.'<div class="cmsect clearfix liked-list end-reached" id="cmsect-my-likes"'.($_GET['cmsect_debug'] == 'true' ? ' data-meta=\''.json_encode($cmsect_args).'\'' : '').'>';
	
	$output_items_HTML = '';
	
	$items = get_posts($cmsect_args);
	if($items
	&& count($liked_items) > 0) {
		foreach($items as $item) {
			
			if(get_post_thumbnail_id($item->ID)) { // if featured image is set
				$featured_image_ID = get_post_thumbnail_id($item->ID);
				$image = wp_get_attachment_image_src($featured_image_ID, CMSECT_THUMBNAIL_IMAGE_SIZE);
			} else { // if featured image is not set, pick first one from attached images
				$attachments = get_posts(array(
					'posts_per_page'=> 1,
					'post_parent'	=> $item->ID,
					'post_type'		=> 'attachment',
					'post_mime_type'=> 'image',
					'orderby'		=> 'ID'
				));
				if($attachments) {
					$attachments = array_values($attachments);
					$image = wp_get_attachment_image_src($attachments[0]->ID, CMSECT_THUMBNAIL_IMAGE_SIZE);
					$featured_image_ID = $attachments[0]->ID;
				}
			}
			unset($image_flip);
			$attachments = get_posts(array(
				'posts_per_page'=> 1,
				'post_parent'	=> $item->ID,
				'post_type'		=> 'attachment',
				'post_mime_type'=> 'image',
				'orderby'		=> 'rand',
				'exclude'		=> $featured_image_ID
			));
			if($attachments) {
				$attachments = array_values($attachments);
				$image_flip = wp_get_attachment_image_src($attachments[0]->ID, CMSECT_THUMBNAIL_IMAGE_SIZE);
			}
			if($item_data_thumbnail_flip_cf) {
				if($custom_flip_image = get_post_meta($item->ID, $item_data_thumbnail_flip_cf, true))
					$image_flip = array($custom_flip_image);
			}
			
			$href = get_permalink($item->ID);
			$excerpt = wp_trim_words(strip_tags(str_replace(']', '>', str_replace('[', '<', $item->post_content))), $item_data_shortdescr_length, '');
			
			$output_items_HTML .=
		'<a href="'.$href.'" class="cmsect-item" data-cmsectitemid="'.$item->ID.'">'
			.($image ? '<span class="cmsect-item-thumb loading'.($image_flip ? ' has-flip-thumb' : '').'" data-cmsectar="'.$image[1].':'.$image[2].'">'
				.'<img src="'.$image[0].'" />'
				.($image_flip
					? '<span class="flip-thumb" style="background-image: url('.$image_flip[0].')"></span>' : '')
			.'</span>' : '')
			.'<span class="cmsect-item-title">'.$item->post_title.'</span>'
			.'<span class="cmsect-item-descr">'.$excerpt.'</span>'
		.'</a>';
			
		}
	}
	
	$output_HTML .= $output_items_HTML
	.'</div><!-- .cmsect -->'
	.'</div><!-- .cmsect-container -->'
	.'<div class="cmsect-clearfix"></div>'
	.'<!-- END: Custom Masonry Section -->';
	
	return $output_HTML;
	
}

add_action('wp_footer', 'cmsect_cust_likes_wp_footer');
function cmsect_cust_likes_wp_footer() {
	if(!is_user_logged_in()) return; ?>
	<style>
	.cmsect-item .cmsect-cust-likes,
	.cmsect-item .cmsect-cust-likes:before,
	.cmsect-item .cmsect-cust-likes:after {
		   -moz-box-sizing: border-box;
		-webkit-box-sizing: border-box;
		     -o-box-sizing: border-box;
		    -ms-box-sizing: border-box;
		        box-sizing: border-box;
	}
	.cmsect-item .cmsect-cust-likes {
		background-color: rgba(0, 0, 0, 0.6);
		display: none;
		height: 100%;
		left: 0;
		position: absolute;
		top: 0;
		width: 100%;
		z-index: 3;
	}
	.cmsect-item:hover .cmsect-cust-likes,
	.cmsect-item.liked .cmsect-cust-likes,
	.cmsect-item.saving-like .cmsect-cust-likes { display: block; }
	.cmsect-item .cmsect-cust-likes:before {
		background: transparent url('<?php echo plugin_dir_url(__FILE__).'images/like_large.png'; ?>') 0 0 no-repeat;
		content: "";
		display: block;
		height: 50px;
		left: 50%;
		margin: -25px 0 0 -26px;
		position: absolute;
		top: 50%;
		width: 52px;
	}
	.cmsect.liked-list .cmsect-item .cmsect-cust-likes:before {
		background-image: url('<?php echo plugin_dir_url(__FILE__).'images/unlike_large.png'; ?>');
	}
	.cmsect-item .cmsect-cust-likes:hover:before,
	.cmsect-item.liked .cmsect-cust-likes:before { opacity: 1 }
	.cmsect-item.liked .cmsect-cust-likes:before { background-position: 0 0; }
	.cmsect-item.saving-like .cmsect-cust-likes:before { display: none }
	.cmsect-item .cmsect-cust-likes::after {
		color: #76cbbf;
		content: "Like";
		display: block;
		font-size: 10px;
		left: 50%;
		letter-spacing: 1px;
		line-height: 10px;
		margin: -7px 0 0 -30px;
		position: absolute;
		text-align: center;
		text-transform: uppercase;
		top: 50%;
		width: 60px;
	}
	.cmsect.liked-list .cmsect-item .cmsect-cust-likes::after {
		color: #ef6657;
		content: "Unlike";
		letter-spacing: 0;
	}
	.cmsect-item.saving-like .cmsect-cust-likes:after {
		   -moz-animation: cmsect-rotation .6s infinite linear;
		-webkit-animation: cmsect-rotation .6s infinite linear;
		     -o-animation: cmsect-rotation .6s infinite linear;
		        animation: cmsect-rotation .6s infinite linear;
		border-color: rgba(149, 242, 229, 0.9) rgba(149, 242, 229, 0.3) rgba(149, 242, 229, 0.3);
		border-radius: 100%;
		border-style: solid;
		border-width: 3px;
		content: "";
		height: 24px;
		left: 50%;
		margin: -12px 0 0 -12px;
		position: absolute;
		top: 50%;
		width: 24px;
	}
	.cmsect.liked-list .cmsect-item.saving-like .cmsect-cust-likes::after {
		content: '';
		border-color: rgba(255, 147, 136, 0.9) rgba(255, 147, 136, 0.3) rgba(255, 147, 136, 0.3);
	}
	.cmsect-item.liked .cmsect-cust-likes::after,
	.cmsect.liked-list .cmsect-item.liked .cmsect-cust-likes::before {
		border-color: #76cbbf;
		border-style: solid;
		border-width: 0 0 3px 3px;
		content: "";
		display: block;
		height: 10px;
		left: 50%;
		margin-left: -11px;
		margin-top: -10px;
		position: absolute;
		top: 50%;
		   -moz-transform: rotate(-45deg);
		-webkit-transform: rotate(-45deg);
		     -o-transform: rotate(-45deg);
		    -ms-transform: rotate(-45deg);
		        transform: rotate(-45deg);
		width: 24px;
	}
	.cmsect.liked-list .cmsect-item.liked .cmsect-cust-likes::after,
	.cmsect.liked-list .cmsect-item.liked .cmsect-cust-likes::before {
		background-color: #ef6657;
		border: 0 none;
		content: "";
		height: 3px;
		margin-top: -4px;
	}
	.cmsect.liked-list .cmsect-item.liked .cmsect-cust-likes::before {
		   -moz-transform: rotate(45deg);
		-webkit-transform: rotate(45deg);
		     -o-transform: rotate(45deg);
		    -ms-transform: rotate(45deg);
		        transform: rotate(45deg);
	}
	.cmsect.no-items-to-show::after {
		color: rgba(0, 0, 0, 0.2);
		content: "There are no items to display here";
		display: block;
		font-size: 30px;
		height: 100%;
		padding-top: 120px;
		position: absolute;
		text-align: center;
		width: 100%;
	}
	</style><script type="text/javascript">
		
		function cmsect_adjust_item_custom_likes(item_el) {
			
			var item_el_cmsect = jQuery(item_el).closest('.cmsect');
			jQuery(item_el)
				.click(function(e) {
					<?php echo apply_filters('cmsect_cust_likes_adjust_item_click_actions_override', '');
					?>if(jQuery(item_el).hasClass('saving-like')
					|| jQuery(item_el).hasClass('liked')) {
						e.preventDefault();
						e.stopPropagation();
						return;
					}
					if(jQuery(e.target).hasClass('cmsect-cust-likes') || 1) { // clicking anywhere saves like
						e.preventDefault();
						e.stopPropagation();
						jQuery(item_el).addClass('saving-like');
						var cmsect_save_like_params = {
							action:	'cmsect_save_like',
							itemid:	jQuery(item_el).data('cmsectitemid'),
							liked: (jQuery(item_el_cmsect).hasClass('liked-list') ? 'false' : 'true')
						};
						jQuery.post(
							cmsect_ajax,
							cmsect_save_like_params,
							function(response) {
								jQuery(item_el).removeClass('saving-like');
								if(response == '1') {
									jQuery(item_el).addClass('liked');
									delay_func(function() {
										
										var $custom_masonry_section = jQuery(item_el_cmsect);
										$custom_masonry_section
											.isotope('remove', jQuery(item_el).parent().find('.cmsect-item.liked'))
											.isotope('layout');
										if(typeof(window['cmsect_adjust_item_custom_likes_callback']) == 'function') {
											window['cmsect_adjust_item_custom_likes_callback'](cmsect_save_like_params);
										}
										delay_func(function() {
											if($custom_masonry_section.find('.cmsect-item').length == 0) {
												$custom_masonry_section.addClass('no-items-to-show');
											}
										}, 1500);
										
									}, 1500);
								}
							}
						);
					}
				})
				.find('.cmsect-item-thumb').append('<span class="cmsect-cust-likes"></span>');
			
		}
		
	</script><?php
}

add_filter('cmsect_items_args', 'cmsect_cust_likes_filter_args');
function cmsect_cust_likes_filter_args($cmsect_args) {
	if(is_user_logged_in()
	&& $user_excludes = cmsect_cust_likes_get_liked_items()) {
		$cmsect_args['exclude'] = implode(',', array_filter(array_unique(array_merge(explode(',', $cmsect_args['exclude']), $user_excludes))));
	}
	return $cmsect_args;
}

add_filter('cmsect_item_href', 'cmsect_cust_likes_filter_item_href', 5, 2);
function cmsect_cust_likes_filter_item_href($href, $item) {
	return 'javascript: void(0)';
}

function cmsect_cust_likes_get_liked_items($user_id = 0) {
	if(is_user_logged_in()) {
		global $wpdb, $current_user;
		get_currentuserinfo();
		if($user = get_user_by('id', $user_id)) {}
		else $user = $current_user;
		return $wpdb->get_col("SELECT item_id FROM {$wpdb->prefix}cms_likes WHERE user_id = ".$user->ID);
	}
	return false;
}

add_action('wp_ajax_cmsect_save_like', 'cmsect_cust_likes_ajax_save_like');
function cmsect_cust_likes_ajax_save_like() {
	if(is_user_logged_in()
	&& is_numeric($_POST['itemid'])
	&& $_POST['itemid'] > 0) {
		global $wpdb, $current_user;
		get_currentuserinfo();
		if($_POST['liked'] == 'true') {
			if($saved = $wpdb->get_row($wpdb->prepare("
	SELECT id
	FROM {$wpdb->prefix}cms_likes
	WHERE user_id = %d AND item_id = %d LIMIT 1",
				$current_user->id, $_POST['itemid']))) {
				die('1');
			} else {
				echo $wpdb->query($wpdb->prepare("
	INSERT INTO {$wpdb->prefix}cms_likes (user_id, item_id) VALUES (%d, %d)", $current_user->id, $_POST['itemid'])) ? '1' : '0';
				exit();
			}
		} else {
			$wpdb->query($wpdb->prepare("
	DELETE FROM {$wpdb->prefix}cms_likes WHERE user_id = %d AND item_id = %d LIMIT 1", $current_user->id, $_POST['itemid']));
			die('1');
		}
	}
}

add_action('delete_post', 'cmsect_cust_likes_delete_post');
function cmsect_cust_likes_delete_post($post_id) {
	global $wpdb;
	$wpdb->query($wpdb->prepare("DELETE FROM {$wpdb->prefix}cms_likes WHERE item_id = %d", $post_id));
}

add_action('delete_user', 'cmsect_cust_likes_delete_user');
function cmsect_cust_likes_delete_user($user_id) {
	global $wpdb;
	$wpdb->query($wpdb->prepare("DELETE FROM {$wpdb->prefix}cms_likes WHERE user_id = %d", $user_id));
}

function cmsect_cust_likes_setup() {
	global $wpdb;
	$structure = 
"CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}cms_likes` (
  `id` bigint(80) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `date_liked` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);";
	$wpdb->query($structure);
}

?>