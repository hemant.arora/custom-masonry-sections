<?php
/**
 * @package Custom Masonry Sections
 * @subpackage Custom Addons
 * @version 0.1
 * 
 * This file is the native customization file
 * for Custom Masonry Sections.
 * 
 */

global $cmsect_custom_modules;

if(file_exists(plugin_dir_path(__FILE__).'/custom.login-for-restricted.php')) {
	require_once plugin_dir_path(__FILE__).'/custom.login-for-restricted.php';
	array_push($cmsect_custom_modules, 'login-for-restricted');
}

if(file_exists(plugin_dir_path(__FILE__).'/custom.likes.php')) {
	require_once plugin_dir_path(__FILE__).'/custom.likes.php';
	array_push($cmsect_custom_modules, 'likes');
}

$custom_dir = plugin_dir_path(__FILE__);
if($custom_dir_handle = opendir($custom_dir)) {
	while($file = readdir($custom_dir_handle)) {
		if(!is_dir($custom_dir.'/'.$file)) {
			$custom_file_slug = str_replace(array('custom.', '.php'), '', $file);
			if(strlen($custom_file_slug) > 0
			&& file_exists($custom_dir.'/custom.'.$custom_file_slug.'.php')
			&& !in_array($custom_file_slug, $cmsect_custom_modules)) {
				require_once $custom_dir.'/custom.'.$custom_file_slug.'.php';
				array_push($cmsect_custom_modules, $custom_file_slug);
			}
		}
	}
}

add_action('wp_footer', 'cmsect_cust_wp_footer');
function cmsect_cust_wp_footer() {
	
	?><script type="text/javascript">
		
		function cmsect_adjust_item_custom(item_el) {
			
			if(typeof(window['cmsect_adjust_item_custom_likes']) == 'function')
				cmsect_adjust_item_custom_likes(item_el);
			
			if(typeof(window['cmsect_adjust_item_custom_lfr']) == 'function')
				cmsect_adjust_item_custom_lfr(item_el);
			
		}
		
	</script><?php
	
}

function cmsect_cust_setup() {
	if(function_exists('cmsect_cust_likes_setup')) {
		cmsect_cust_likes_setup();
	}
}

?>