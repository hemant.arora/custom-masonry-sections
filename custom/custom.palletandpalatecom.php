<?php
/**
 * @package Custom Masonry Sections
 * @subpackage For PalletandPalate.com
 * @version 0.1
 */

define('CMSECT_CUST_PNP_HOWDY_TEXT_TEMPLATE', 'Howdy, {display_name}');
define('CMSECT_CUST_DEFAULT_ITEMS_TYPE', 'design');

global $cmsect_custom_modules_global_admin_fields;
$cmsect_custom_modules_global_admin_fields['palletandpalatecom'] = 'cmsect_cust_pnp_global_admin_fields';

function cmsect_cust_pnp_global_admin_fields() {
	global $cms_settings;
	?><div class="field">
		<label class="title">ADD-ON: PalletandPalate.com Options</label>
		<span class="descr mrgn-b-10">Please manage below the custom options and settings for the website PalletandPalate.com</span>
		
		<div class="field-option mrgn-b-10 hidden" style="display: block">
			<div class="sub-option mrgn-l-20" style="display: block">
				<label>Wishlist Page</label>
				<span class="descr">URL</span>
				<input type="text" name="cms_settings[pnp_likes_page_url]" value="<?php echo $cms_settings['pnp_likes_page_url']; ?>" />
				<span class="descr">Link Text</span>
				<input type="text" name="cms_settings[pnp_likes_page_link_text]" value="<?php echo $cms_settings['pnp_likes_page_link_text']; ?>" />
			</div>
		</div>
		
		<div class="field-option mrgn-b-10 hidden" style="display: block">
			<div class="sub-option mrgn-l-20" style="display: block">
				<label>Account Page</label>
				<span class="descr">URL</span>
				<input type="text" name="cms_settings[pnp_account_page_url]" value="<?php echo $cms_settings['pnp_account_page_url']; ?>" />
				<span class="descr">Link Text</span>
				<input type="text" name="cms_settings[pnp_account_page_link_text]" value="<?php echo $cms_settings['pnp_account_page_link_text']; ?>" />
			</div>
		</div>
	</div><?php
}

add_action('wp_print_scripts', function() {
	?>
<style>
#cmsect_cust_lfr_site_logo { overflow: hidden }
#cmsect_cust_lfr_site_logo:before {
    background: transparent url("http://tivlabs.us/pNp/wp-content/uploads/2015/10/dark-logo.png") no-repeat scroll center center;
    background-size: contain;
    content: "";
    display: block;
    height: 220px;
    left: 50%;
    margin: -106px 0 0 -200px;
    opacity: 0.1;
    position: absolute;
    top: 50%;
    width: 500px;
    z-index: 0;
}
.cmsect_cust_popup_overlay { z-index: 1000 !important }
#cmsect_cust_lfr_site_logo .cmsect_cust_lfr_login_label {
    color: #000;
    text-transform: uppercase;
}
#cmsect_cust_lfr_login label input[type="text"], #cmsect_cust_lfr_login label input[type="password"] {
    box-sizing: border-box;
    padding: 12px;
}
#cmsect_cust_lfr_login button[type="submit"] {
	background-color: #1abc9c !important;
	font-family: Roboto, sans-serif;
	font-size: 16px;
	height: 51px;
    letter-spacing: 1px;
    padding: 14px;
    text-transform: uppercase;
}
#cmsect_cust_lfr_login * { box-sizing: border-box }

#cmsect_cust_lfr_login label.has-links-signup-forgot a.cmsect_cust_lft_link_signup,
#cmsect_cust_lfr_login label.has-links-signup-forgot a.cmsect_cust_lft_link_forgot {
    background-color: #96efe0;
    color: #469f90;
    font-size: 12px;
}
#cmsect_cust_lfr_login label.has-links-signup-forgot a.cmsect_cust_lft_link_signup:hover,
#cmsect_cust_lfr_login label.has-links-signup-forgot a.cmsect_cust_lft_link_forgot:hover { background-color: #a6fff0; }
#cmsect_cust_lfr_login button[type="submit"]:hover { background-color: #2accac !important }

nav.main_menu .cmsect_cust_pnp_howdy { position: relative }
nav.main_menu .cmsect_cust_pnp_howdy > ul {
    background-color: #fff;
    border: 1px solid rgba(0, 0, 0, 0.2);
    border-radius: 5px;
    display: none;
    margin-top: -10px;
    position: absolute;
    right: 0;
    top: 100%;
    width: 150px;
    z-index: 10;
}
nav.main_menu .cmsect_cust_pnp_howdy > ul li {
    display: block;
    float: none;
}
nav.main_menu .cmsect_cust_pnp_howdy > ul li a {
    display: block;
    font-size: 13px;
    line-height: 40px;
    padding: 0 10px;
    position: relative;
    text-transform: uppercase;
}
nav.main_menu .cmsect_cust_pnp_howdy > ul li:first-child a { border-radius: 4px 4px 0 0; }
nav.main_menu .cmsect_cust_pnp_howdy > ul li:last-child a { border-radius: 0 0 4px 4px; }
nav.main_menu .cmsect_cust_pnp_howdy > ul li a:hover {
    background-color: #f5f5f5;
    color: #000;
}
nav.main_menu .cmsect_cust_pnp_howdy > ul li:first-child a::before {
    border-bottom: 7px solid #ccc;
    border-left: 6px solid transparent;
    border-right: 6px solid transparent;
    content: "";
    display: block;
    position: absolute;
    right: 20px;
    top: -7px;
}
nav.main_menu .cmsect_cust_pnp_howdy > ul li:first-child a::after {
    border-bottom: 5px solid #fff;
    border-left: 4px solid transparent;
    border-right: 4px solid transparent;
    content: "";
    display: block;
    position: absolute;
    right: 22px;
    top: -5px;
}
nav.main_menu .cmsect_cust_pnp_howdy > ul li:first-child a:hover::after { border-bottom-color: #f5f5f5 }	
</style><?php
}, 1000);

add_action('wp_footer', 'cmsect_cust_pnp_wp_footer', 1000);
function cmsect_cust_pnp_wp_footer() {
	if(is_user_logged_in()) {
		global $current_user;
		get_currentuserinfo();
		
		$cmsect_cust_pnp_loggedin_menu_drop_links = array();
		$likes_page_link = array();
		$account_page_link = array();
		$cmsect_sections = cmsect_get_all();
		$cms_settings = cmsect_get_settings();
		if(is_array($cmsect_sections)) {
			foreach($cmsect_sections as $cmsect_section) {
				if($cms_settings['pnp_likes_page_link_text']) {
					array_push($cmsect_cust_pnp_loggedin_menu_drop_links, array(
						'text'	=> $cms_settings['pnp_likes_page_link_text'],
						'url'	=> $cms_settings['pnp_likes_page_url']
					));
				}
				if($cms_settings['pnp_account_page_link_text']) {
					array_push($cmsect_cust_pnp_loggedin_menu_drop_links, array(
						'text'	=> $cms_settings['pnp_account_page_link_text'],
						'url'	=> $cms_settings['pnp_account_page_url']
					));
				}
			}
		}
		$cmsect_cust_pnp_loggedin_menu_drop_links = apply_filters('cmsect_cust_pnp_loggedin_menu_drop_links', $cmsect_cust_pnp_loggedin_menu_drop_links);
		
	?><script type="text/javascript">
		
		var cmsect_cust_lfr_logout_btn_setup_callback_executed = false;
		function cmsect_cust_lfr_logout_btn_setup_callback() {
			if(cmsect_cust_lfr_logout_btn_setup_callback_executed == true) return;
			cmsect_cust_lfr_logout_btn_setup_callback_executed = true;
			<?php if(false === $cmsect_cust_pnp_loggedin_menu_drop_links) { ?>return;<?php } ?>
			jQuery('header .cmsect-cust-lfr-logout-button').parent().addClass('cmsect_cust_pnp_howdy');
			jQuery('header .cmsect-cust-lfr-logout-button').after(
				'<a href="javascript: void(0)"><?php echo str_replace('{display_name}', $current_user->data->display_name, CMSECT_CUST_PNP_HOWDY_TEXT_TEMPLATE); ?></a>'
				+'<ul style="display: none">'
					<?php if(!empty($cmsect_cust_pnp_loggedin_menu_drop_links)) foreach($cmsect_cust_pnp_loggedin_menu_drop_links as $ccplmdl) { ?>
					+'<li><a href="<?php echo $ccplmdl['url']; ?>"><?php echo $ccplmdl['text']; ?></a></li>'<?php } ?>
				+'</ul>'
			);
			jQuery('header .cmsect-cust-lfr-logout-button').insertAfter(jQuery('.cmsect_cust_pnp_howdy ul li:last-child'));
			jQuery('header .cmsect-cust-lfr-logout-button').wrap('<li></li>');
			jQuery('.cmsect_cust_pnp_howdy > a').click(function(e) {
				e.preventDefault();
				jQuery(this).closest('.cmsect_cust_pnp_howdy').find('ul').fadeIn('fast');
			});
			jQuery('html, body').click(function(e) {
				if(jQuery(e.target).parents('.cmsect_cust_pnp_howdy').length > 0
				|| jQuery(e.target).hasClass('cmsect_cust_pnp_howdy')) {}
				else {
					jQuery('.cmsect_cust_pnp_howdy').find('ul').fadeOut('fast');
				}
			});
		}
		
	</script><?php
	}
}

add_action('admin_print_scripts', function() {
?><style>
#menu-posts-testimonials,
#menu-posts-masonry_gallery,
#menu-posts-portfolio_page {
	display: none
}
.frm_cmsect_cust_likes_wishlist .has-cmsect-post-type { display: none !important }
</style><?php
}, 9999);

?>