<?php
/**
 * @package Custom Masonry Sections
 * @subpackage Login for Restricted
 * @version 0.1
 */

define('CMSECT_CUST_LFR_LOGIN_LOGOUT_ANCHOR_WORD', '#cmsect-login-logout');

global
	$cmsect_custom_modules_global_admin_fields,
	$cmsect_custom_modules_admin_fields;
$cmsect_custom_modules_global_admin_fields['login-for-restricted'] = 'cmsect_cust_lfr_global_admin_fields';
$cmsect_custom_modules_admin_fields['login-for-restricted'] = 'cmsect_cust_lfr_admin_fields';

function cmsect_cust_lfr_global_admin_fields() {
	global $cms_settings;
	?><div class="field">
		<label class="title">ADD-ON: Restrict Access</label>
		<span class="descr mrgn-b-10">The settings below will apply to masonry sections where you enable access restriction.</span>
		
		<div class="field-option mrgn-b-10 hidden" style="display: block">
			<div class="sub-option mrgn-l-20" style="display: block">
				<label>Item attributes when user is not logged in. All fields below are optional.</label>
				<span class="descr">Item Link (href)</span>
				<input type="text" name="cms_settings[restricted_href]" value="<?php echo $cms_settings['restricted_href']; ?>" />
				<span class="descr">Item Class</span>
				<input type="text" name="cms_settings[restricted_class]" value="<?php echo $cms_settings['restricted_class']; ?>" />
				<span class="descr">Item Class (Hover)</span>
				<input type="text" name="cms_settings[restricted_class_hover]" value="<?php echo $cms_settings['restricted_class_hover']; ?>" />
			</div>
		</div>
		
		<div class="field-option mrgn-b-10 hidden" style="display: block">
			<div class="sub-option mrgn-l-20" style="display: block">
				<label>Login/Logout button</label>
				<span class="descr mrgn-b-10">To place a Login/Logout button in the nav menu, please add a new link to the menu of your choice and mention the link/href as <code><?php echo CMSECT_CUST_LFR_LOGIN_LOGOUT_ANCHOR_WORD; ?></code>. This href can be used with any anchor <code>&lt;a&gt;</code> tag on your website.</span>
			</div>
		</div>
		
		<div class="field-option mrgn-b-10 hidden" style="display: block">
			<div class="sub-option mrgn-l-20" style="display: block">
				<label>Please provide links to SignUp and Forgot Password pages below:</label>
				<span class="descr">SignUp Page URL</span>
				<input type="text" name="cms_settings[restricted_signup_url]" value="<?php echo $cms_settings['restricted_signup_url']; ?>" />
				<span class="descr">Forgot Password URL<br /><em>Leave blank to use built-in password reset feature.</em></span>
				<input type="text" name="cms_settings[restricted_forgot_url]" value="<?php echo $cms_settings['restricted_forgot_url']; ?>" />
			</div>
		</div>
	</div><?php
}
function cmsect_cust_lfr_admin_fields() {
	global $cms_section;
	?><div class="field">
		<label class="title" for="cms_section_is_restricted"><input type="checkbox" name="cms_section[is_restricted]" id="cms_section_is_restricted" value="Y"<?php echo $cms_section->options['is_restricted'] == 'Y' ? ' checked=""' : ''; ?> /> ADD-ON: Restrict Access</label>
		<span class="descr">
			Please check this option if you want only logged in users to access these items.
			<br /><em>Please check the 'Settings' page for more options.</em>
		</span>
	</div><?php
}

add_action('wp_footer', 'cmsect_cust_lfr_wp_footer');
function cmsect_cust_lfr_wp_footer() {
	
	/* Removed restriction to allow login/logout link to work site-wide
	if(!is_user_logged_in()
	&& $cmsect_sections = cmsect_get_all()) {*/
	$signup_url = 'javascript: void(0)';
	$forgot_url = '#forgot-password';
	$cmsect_sections = cmsect_get_all();
	$cms_settings = cmsect_get_settings();
	if($cmsect_sections) {
		foreach($cmsect_sections as $cmsect_section) {
			$signup_url = $cms_settings['restricted_signup_url'] ? $cms_settings['restricted_signup_url'] : $signup_url;
			$forgot_url = $cms_settings['restricted_forgot_url'] ? $cms_settings['restricted_forgot_url'] : $forgot_url;
		}
	} ?>
	<style>
	.cmsect_cust_popup_overlay,
	.cmsect_cust_popup_overlay * { outline: none !important }
	.cmsect_cust_popup_overlay {
		background-color: rgba(0, 0, 0, 0.8);
		display: none;
		height: 100%;
		left: 0;
		position: fixed;
		top: 0;
		width: 100%;
		z-index: 50;
	}
	#cmsect_cust_lfr_login {
		background-color: #fff;
		border-radius: 5px;
		display: none;
		height: 300px;
		left: 50%;
		margin: -150px 0 0 -150px;
		position: absolute;
		top: 50%;
		width: 300px;
	}
	
	#cmsect_cust_lfr_site_logo {
		border-bottom: rgba(0,0,0,0.1) 1px solid;
		display: block;
		height: 50px;
		position: relative;
	}
	#cmsect_cust_lfr_site_logo .cmsect_cust_lfr_login_label {
		display: block;
		line-height: 50px;
		text-align: center;
	}
	#cmsect_cust_lfr_site_logo .cmsect_cust_lfr_login_label:before {
		background: transparent url('<?php echo CMSECT_BASE_URL.'custom/images/lock.png'; ?>') 0 0 no-repeat;
		content: "";
		display: inline-block;
		height: 16px;
		margin-right: 4px;
		width: 16px;
	}
	
	#cmsect_cust_lfr_login label { display: block; padding: 4px 20px }
	#cmsect_cust_lfr_login label input[type="text"],
	#cmsect_cust_lfr_login label input[type="password"] { width: 100%; }
	
	#cmsect_cust_lfr_login button[type="submit"] {
		background-color: #666;
		border-radius: 0 0 3px 3px;
		border: 0 none;
		bottom: 0;
		color: #FFF;
		font-weight: bold;
		position: absolute;
		text-align: center;
		width: 100%;
	}
	#cmsect_cust_lfr_login button[type="submit"]:hover { background-color: #444 }
	.cmsect_cust_lfr_error { color: #E77; display: none; left: 0; padding: 50px 20px; position: absolute; top: 50px; width: 100%; }
	.cmsect_cust_lfr_error .cmsect_cust_lfr_success_msg { color: #4B4 }
	
	#cmsect_cust_lfr_login label.has-links-signup-forgot {
		bottom: 50px;
		padding: 0;
		position: absolute;
		width: 100%;
	}
	label.has-links-signup-forgot a.cmsect_cust_lft_link_signup,
	label.has-links-signup-forgot a.cmsect_cust_lft_link_forgot {
		background-color: #CCC;
		bottom: 0;
		font-size: 14px;
		left: 0;
		padding: 4px 0;
		position: absolute;
		text-align: center;
		width: 50%;
	}
	label.has-links-signup-forgot a.cmsect_cust_lft_link_forgot { left: 50% }
	label.has-links-signup-forgot a.cmsect_cust_lft_link_signup:hover,
	label.has-links-signup-forgot a.cmsect_cust_lft_link_forgot:hover { background-color: #EEE; }
	</style>
	<div class="cmsect_cust_popup_overlay">
		<form id="cmsect_cust_lfr_login" class="clearfix" method="post" action="">
			<input type="hidden" name="action" value="cmsect_cust_lfr_do_login" />
			<div id="cmsect_cust_lfr_site_logo">
				<span class="cmsect_cust_lfr_login_label">Account Login</span>
			</div>
			<label class="has-username">
				<span><?php echo apply_filters('cmsect_cust_lfr_login_box_username_text', 'Username'); ?></span>
				<input type="text" name="log" autocomplete="off" id="user_login" placeholder="<?php echo apply_filters('cmsect_cust_lfr_login_box_username_text', 'Username'); ?>" />
			</label>
			<label class="has-password">
				<span><?php echo apply_filters('cmsect_cust_lfr_login_box_password_text', 'Password'); ?></span>
				<input type="password" name="pwd" autocomplete="off" id="user_pass" placeholder="<?php echo apply_filters('cmsect_cust_lfr_login_box_password_text', 'Password'); ?>" />
			</label>
			<label class="has-rememberme" style="display: none"><input id="rememberme" type="checkbox" value="forever" name="rememberme" checked="" /> Remember Me</label>
			<label class="has-links-signup-forgot">
				<a href="<?php echo $signup_url; ?>" class="cmsect_cust_lft_link_signup">Sign Up</a>
				<a href="<?php echo $forgot_url; ?>" class="cmsect_cust_lft_link_forgot">Forgot Password?</a>
			</label>
			<button type="submit">Log In</button>
		</form>
	</div>
	<script type="text/javascript">
		
		var cmsect_cust_lfr_classes = new Array(),
			cmsect_cust_lfr_forgot_password_animating = false;
	<?php if($cmsect_sections) {
		foreach($cmsect_sections as $cmsect_section) { ?>
		cmsect_cust_lfr_classes['cmsect-'+<?php echo $cmsect_section->id; ?>] = '<?php echo $cms_settings['restricted_class']; ?>';
	<?php } } ?>
		
		jQuery(document).ready(function() {
			jQuery('html, body').click(function(e) {
				if(jQuery(e.target).hasClass('cmsect_cust_popup_overlay')) {
					jQuery('#cmsect_cust_lfr_login').fadeOut('fast', function() {
						jQuery('.cmsect_cust_popup_overlay').fadeOut('fast');
					});
				}
			});
			jQuery('#cmsect_cust_lfr_login').submit(function(e) {
				e.preventDefault();
				var self = jQuery(this);
				var is_forgot_pass = jQuery(this).hasClass('forgot-password');
				if(jQuery(self).hasClass('submitting')) return false;
				jQuery(self).addClass('submitting').find('[type="submit"]').addClass('loading');
				jQuery.post(
					cmsect_ajax,
					{
						action:		is_forgot_pass ? 'cmsect_do_reset_pass' : 'cmsect_do_login',
						log:		jQuery('#cmsect_cust_lfr_login input[name="log"]').val(),
						pwd:		jQuery('#cmsect_cust_lfr_login input[name="pwd"]').val(),
						rememberme:	jQuery('#cmsect_cust_lfr_login input[name="rememberme"]').is(':checked')
					},
					function(response) {
						jQuery(self).removeClass('submitting').find('[type="submit"]').removeClass('loading');
						var error_msg = (is_forgot_pass ? 'The entered username/email is not registered' : 'The entered username and password do not match')
							+'. Please check your entry and retry.';
						if(response == '1') {
							error_msg = '<span class="cmsect_cust_lfr_success_msg">'+(is_forgot_pass ? 'A new password has been emailed to you. Please check your inbox.' : '<?php echo apply_filters('cmsect_cust_lfr_logged_in_message', 'You are being logged in. Please wait...'); ?>')+'</span>';
						} else if(response != '0') {
							error_msg = response;
						}
						jQuery(self).append('<div class="cmsect_cust_lfr_error">'+error_msg+'</div>');
						jQuery(self).find('label, [type="submit"]').not('.has-rememberme').fadeOut('fast', function() {
							if(response == '1' && !is_forgot_pass) {
								jQuery(self).find('.cmsect_cust_lfr_error').fadeIn('fast');
								<?php echo apply_filters('cmsect_cust_lfr_logged_in_redirect_js', 'window.location.reload();'); ?>
								return;
							} else {
								jQuery(self).find('.cmsect_cust_lfr_error').fadeIn('fast').delay(5000).fadeOut('fast', function() {
									jQuery(self).find('label, [type="submit"]').not('.has-rememberme'+(is_forgot_pass ? ', .has-password' : '')).fadeIn('fast');
									jQuery(self).find('.cmsect_cust_lfr_error').empty().remove();
								});
							}
						});
					}
				);
			});
			// Login/Logout button
			jQuery('a[href="<?php echo CMSECT_CUST_LFR_LOGIN_LOGOUT_ANCHOR_WORD; ?>"]').each(function(i, el) {
		<?php if(is_user_logged_in()) { ?>
				jQuery(el).parent().addClass('has-cmsect-cust-lfr-logout');
				jQuery(el).html('Logout').attr('href', '#logout').addClass('cmsect-cust-lfr-logout-button').click(function(e) {
					e.preventDefault();
					jQuery('#cmsect_cust_lfr_login').append('<div class="cmsect_cust_lfr_error"><?php echo apply_filters('cmsect_cust_lfr_logged_out_message', 'You are being logged out. Please wait...'); ?></div>');
					jQuery('#cmsect_cust_lfr_login .cmsect_cust_lfr_error').show();
					jQuery('#cmsect_cust_lfr_login').find('label, [type="submit"]').not('.has-rememberme').hide();
					jQuery('.cmsect_cust_popup_overlay').fadeIn('fast', function() {
						jQuery('#cmsect_cust_lfr_login').fadeIn('fast');
					});
					jQuery.post(
						cmsect_ajax,
						{
							action:		'cmsect_do_logout'
						},
						function(response) {
							if(response == '1') window.location.reload();
							else {
								jQuery('#cmsect_cust_lfr_login .cmsect_cust_lfr_error').fadeOut('fast', function() {
									jQuery(this).html('An unexpected error has occured when trying to log you out.');
									jQuery(this).fadeIn('fast').delay(5000).fadeOut('fast', function() {
										jQuery('#cmsect_cust_lfr_login').find('label, [type="submit"]').not('.has-rememberme').show();
										jQuery('#cmsect_cust_lfr_login').find('.cmsect_cust_lfr_error').empty().remove();
										jQuery('#cmsect_cust_lfr_login').fadeOut('fast', function() {
											jQuery('.cmsect_cust_popup_overlay').fadeOut('fast');
										});
									});
								});
							}
						}
					);
				});
				if(typeof(window['cmsect_cust_lfr_logout_btn_setup_callback']) == 'function') window['cmsect_cust_lfr_logout_btn_setup_callback']();
		<?php } else { ?>
				jQuery(el).html('Login').attr('href', '#login').addClass('cmsect-cust-lfr-login-button').click(function(e) {
					e.preventDefault();
					jQuery('.cmsect_cust_popup_overlay').fadeIn('fast', function() {
						jQuery('#cmsect_cust_lfr_login').fadeIn('fast');
					});
				});
		<?php } ?>
			});
			// Change form to Forgot password...
			jQuery('a[href="#forgot-password"]').click(function(e) {
				e.preventDefault();
				if(cmsect_cust_lfr_forgot_password_animating == true) return;
				cmsect_cust_lfr_forgot_password_animating = true;
				var frm = jQuery(this).closest('#cmsect_cust_lfr_login');
				if(jQuery(frm).hasClass('forgot-password')) {
					jQuery(frm).removeClass('forgot-password');
					jQuery(frm).find('label.has-username').animate({ marginTop: 0 }, 'fast', function() {
						jQuery(frm).find('label.has-username span').html('<?php echo apply_filters('cmsect_cust_lfr_login_box_username_text', 'Username'); ?>').next('input').attr('placeholder', '<?php echo apply_filters('cmsect_cust_lfr_login_box_username_text', 'Username'); ?>').val('');
						jQuery(frm).find('label.has-password').fadeIn('fast', function() {
							cmsect_cust_lfr_forgot_password_animating = false;
						});
						jQuery(frm).find('.cmsect_cust_lft_link_forgot').animate({ left: '50%', width: '50%' }, 'fast', function() { jQuery(this).html('Forgot Password?'); jQuery(this).closest('form').find('[type="submit"]').html('Log In'); });
					});
				} else {
					jQuery(frm).addClass('forgot-password');
					var username_margin_top = Math.floor(jQuery(frm).find('label.has-password').height() / 2);
					jQuery(frm).find('label.has-password').fadeOut('fast', function() {
						jQuery(frm).find('label.has-username span').html('Email').next('input').attr('placeholder', 'Email').val('');
						jQuery(frm).find('label.has-username').animate({ marginTop: username_margin_top }, 'fast', function() {
							cmsect_cust_lfr_forgot_password_animating = false;
						});
						jQuery(frm).find('.cmsect_cust_lft_link_forgot').animate({ left: 0, width: '100%' }, 'fast', function() { jQuery(this).html('Cancel'); jQuery(this).closest('form').find('[type="submit"]').html('Reset My Password'); });
					});
				}
			});
		});
		
		function cmsect_adjust_item_custom_lfr(item_el) {
			
			var cmsect_id = jQuery(item_el).closest('.cmsect').attr('id');
			for(property in cmsect_cust_lfr_classes) {
				if(property == cmsect_id
				&& jQuery(item_el).hasClass(cmsect_cust_lfr_classes[property])) {
					
					jQuery(item_el).click(function(e) {
						e.preventDefault();
						jQuery('.cmsect_cust_popup_overlay').fadeIn('fast', function() {
							jQuery('#cmsect_cust_lfr_login').fadeIn('fast');
						});
					});
					
				}
			}
			
		}
		
	</script><?php
	
}

add_action('wp_ajax_nopriv_cmsect_do_login', 'cmsect_cust_lfr_do_login');
function cmsect_cust_lfr_do_login() {
	$creds = array();
	$creds['user_login'] = $_POST['log'];
	$creds['user_password'] = $_POST['pwd'];
	$creds['remember'] = $_POST['rememberme'] == 'true' ? true : false;
	$user = wp_signon($creds, false);
	if(is_wp_error($user)) {
		$error_msg = $user->get_error_message() ? $user->get_error_message() : 'The username and password do not match. Please check your entry and retry.';
		echo apply_filters('cmsect_cust_lfr_do_login_error_msg', $error_msg);
	} else
		echo '1';
	exit();
}

add_action('wp_ajax_nopriv_cmsect_do_reset_pass', 'cmsect_cust_lfr_do_reset_pass');
function cmsect_cust_lfr_do_reset_pass() {
	
	$error = '';
	
	$email = trim($_POST['log']);
	
	if(empty($email)) {
		$error = 'Please enter your e-mail address.';
	} elseif(!is_email($email)) {
		$error = 'The entered e-mail address is not valid.';
	} elseif(!email_exists($email)) {
		$error = 'There is no user registered with that email address.';
	} else {
		
		$random_password = wp_generate_password(12, false);
		
		$user = get_user_by('email', $email);
		
		$update_user = wp_update_user(array(
			'ID' => $user->ID, 
			'user_pass' => $random_password
		));
		
		if($update_user) {
			$to = $email;
			$subject = apply_filters('cmsect_cust_lfr_reset_password_email_subject', 'Your new password', $user->display_name);
			
			$message = apply_filters('cmsect_cust_lfr_reset_password_email_body', 'Your new password is: '.$random_password, $user->display_name, $random_password);
			
			$headers[] = 'MIME-Version: 1.0' . "\r\n";
			$headers[] = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers[] = "X-Mailer: PHP \r\n";
			
			$mail = wp_mail($to, $subject, $message, $headers);
			if($mail)
				die('1');
		} else {
			$error = apply_filters('cmsect_cust_lfr_reset_password_error_msg', 'Oops! Something went wrong when updating your account. Please contact us at <a href="mailto:'.get_option('admin_email').'">'.get_option('admin_email').'</a>');
		}
		
	}
	if($error) die($error);
}

add_action('wp_ajax_cmsect_do_logout', 'cmsect_cust_lfr_do_logout');
function cmsect_cust_lfr_do_logout() {
	wp_logout();
	die('1');
}

?>