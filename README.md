-----------------------
Custom Masonry Sections
-----------------------

Author: Hemant Arora
Company: TivLabs

Custom Masonry Sections is a flexible WordPress plugin that allows website
owners to display a customizable masonry of items (posts, or custom post types)
on a page of their choice. This is made possible via use of shortcodes.

Once a section has been saved, its shortcode can be used on any WordPress
Post/Page/custom post or even a widget supporting shortcodes.

A page can even have multiple 'Custom Masonry Sections' shortcodes.

The sections are highly customizable, allowing you to select the specific
post type to show items from, filter/exclude specific items or even those under
a category/term, sort items the way you want, display filters above the
masonry to filter items in real-time, add custom CSS to match your theme,
and even restrict access to the items for not registered users.

The masonry also features infinite scroll and you can choose the exact number
of items that you wish to show per page (upon every fetch).


Usage
-----

Once you have created a section, its shortcode gets generated. The shortcode
is of the form:

[cmsect id="CMSECT_SECTION_ID"]

e.g. [cmsect id="24"]

Multiple 'Custom Masonry Sections' shortcodes can be added to the same post,
page, or other post type.


Add-Ons
-------

Custom Masonry Sections offers flexibility and scalability by offering a set
of add-ons. The add-ons can be added/removed in/from the directory 'custom'.
While the custom controler file 'custom/custom.php' stays put, specific add-on
files can be simply put in or removed from the custom directory to enable or
disable the respective feature.
