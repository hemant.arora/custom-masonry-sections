<?php
/*
Plugin Name: Custom Masonry Sections
Plugin URI: http://www.tivlabs.com/
Description: Create custom sections with items listed as masonry bricks.
Version: 0.1
Author: TivLabs
Author URI: http://www.tivlabs.com/
*/

define('CMSECT_GUTTER_SIZE_PX', 10);
define('CMSECT_THUMBNAIL_IMAGE_SIZE', 'full'); // medium
define('CMSECT_BASE_URL', plugin_dir_url(__FILE__));

require_once 'functions_admin.php';
require_once 'functions_shortcode.php';

global
	$cmsect_custom_modules,
	$cmsect_custom_modules_global_admin_fields,
	$cmsect_custom_modules_admin_fields;
$cmsect_custom_modules = array();
$cmsect_custom_modules_global_admin_fields = array();
$cmsect_custom_modules_admin_fields = array();
if(file_exists(plugin_dir_path(__FILE__).'/custom/custom.php'))
require_once plugin_dir_path(__FILE__).'/custom/custom.php';

register_activation_hook(__FILE__, 'cmsect_setup');

?>