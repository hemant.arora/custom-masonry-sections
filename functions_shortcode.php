<?php
/**
 * @package Custom Masonry Sections
 * @version 0.1
 */

global $cmsect_shortcodes_list;
$cmsect_shortcodes_list = array(
	'cmsect'
);

function cmsect_get_shortcode($cms_id) {
	return '[cmsect id="'.$cms_id.'"]';
}

add_shortcode('cmsect', 'cmsect_shortcode');
function cmsect_shortcode($atts) {
	$atts = shortcode_atts(array(
		'id' => 0,
		'offset' => 0,
		'exclude' => ''
	), $atts, 'cmsect');
	
	extract($atts);
	
	$cms_settings = cmsect_get_settings();
	
	if($cms_section = cmsect_get($id)) {
		
		$is_restricted = ($cms_section->options['is_restricted'] == 'Y' && !is_user_logged_in()) ? true : false;
		
		$output_HTML = '<!-- START: Custom Masonry Section -->'
		.'<div class="cmsect-clearfix"></div>'
		.'<div class="cmsect-container">'
		.'<div class="cmsect clearfix" id="cmsect-'.$cms_section->id.'"'
			.($is_restricted && $cms_settings['restricted_class_hover'] ? ' data-restrictedclasshover="'.$cms_settings['restricted_class_hover'].'"' : '').'>';
		
		$output_items_HTML = '';
		
		$items_orderby = 'date';
		$items_order = 'DESC';
		if($cms_section->options['sort_items']) {
			switch($cms_section->options['sort_items']) {
				case 'date_asc':
					$items_orderby = 'date';
					$items_order = 'ASC';
				break;
				case 'mod_desc':
					$items_orderby = 'modified';
					$items_order = 'DESC';
				break;
				case 'mod_asc':
					$items_orderby = 'modified';
					$items_order = 'ASC';
				break;
				case 'title_asc':
					$items_orderby = 'title';
					$items_order = 'ASC';
				break;
				case 'comm_desc':
					$items_orderby = 'comment_count';
					$items_order = 'DESC';
				break;
				case 'comm_asc':
					$items_orderby = 'comment_count';
					$items_order = 'ASC';
				break;
				case 'rand':
					$items_orderby = 'rand';
				break;
				case 'date_desc':
				default:
					$items_orderby = 'date';
					$items_order = 'DESC';
				break;
			}
		}
		
		if(isset($cms_section->options['filter_by']['category'])) {
			if(is_array($cms_section->options['filter_by']['category']))
				$cms_section->options['filter_by']['category'] = implode(',', $cms_section->options['filter_by']['category']);
		}
		
		$exclude_list = explode(',', $exclude);
		if(count($exclude_list) > 0/* && $items_orderby == 'rand'*/) $offset = 0;
		if(isset($cms_section->options['filter_by']['exclude_ids'])
		&& $cms_section->options['filter_items'] == 'Y') {
			$cms_section->options['filter_by']['exclude_ids'] = trim($cms_section->options['filter_by']['exclude_ids']);
			$cms_section->options['filter_by']['exclude_ids'] = explode(',', $cms_section->options['filter_by']['exclude_ids']);
			for($x = 0; $x < count($cms_section->options['filter_by']['exclude_ids']); $x++)
				$cms_section->options['filter_by']['exclude_ids'][$x] = trim($cms_section->options['filter_by']['exclude_ids'][$x]);
			$cms_section->options['filter_by']['exclude_ids'] = implode(',', $cms_section->options['filter_by']['exclude_ids']);
			if(count($cms_section->options['filter_by']['exclude_ids']) > 0)
				$exclude_list = array_merge($exclude_list, $cms_section->options['filter_by']['exclude_ids']);
		}
		$exclude_list = array_filter(array_unique($exclude_list));
		
		$cms_section->options['item_data_shortdescr_length'] = is_numeric($cms_section->options['item_data_shortdescr_length']) ? $cms_section->options['item_data_shortdescr_length'] : 20;
		
		$cms_section->options['item_data'] = is_array($cms_section->options['item_data']) ? $cms_section->options['item_data'] : array();
		
		$cmsect_args = array(
			'posts_per_page'   => $cms_section->options['items_per_page'],
			'offset'           => $offset,
			'category'         => ($cms_section->options['filter_items'] == 'Y' && $cms_section->options['filter_by']['category'] ? $cms_section->options['filter_by']['category'] : ''),/*
			'category_name'    => '',*/
			'orderby'          => $items_orderby,
			'order'            => $items_order,/*
			'include'          => '',*/
			'exclude'          => implode(',', $exclude_list),/*
			'meta_key'         => '',
			'meta_value'       => '',*/
			'post_type'        => ($cms_section->options['items_type'] == 'custom' ? $cms_section->options['items_type_custom'] : $cms_section->options['items_type']),/*
			'post_mime_type'   => '',
			'post_parent'      => '',
			'author'           => '',*/
			'post_status'      => 'publish'
		);
		$cmsect_args = apply_filters('cmsect_items_args', $cmsect_args);
		
		$items = get_posts($cmsect_args);
		
		if(is_array($items)
		&& count($cms_section->options['item_data']) > 0) {
			foreach($items as $item) {
				
				if(in_array('thumbnail', $cms_section->options['item_data'])) {
					if(get_post_thumbnail_id($item->ID)) { // if featured image is set
						$featured_image_ID = get_post_thumbnail_id($item->ID);
						$image = wp_get_attachment_image_src($featured_image_ID, CMSECT_THUMBNAIL_IMAGE_SIZE);
					} else { // if featured image is not set, pick first one from attached images
						$attachments = get_posts(array(
							'posts_per_page'=> 1,
							'post_parent'	=> $item->ID,
							'post_type'		=> 'attachment',
							'post_mime_type'=> 'image',
							'orderby'		=> 'ID'
						));
						if($attachments) {
							$attachments = array_values($attachments);
							$image = wp_get_attachment_image_src($attachments[0]->ID, CMSECT_THUMBNAIL_IMAGE_SIZE);
							$featured_image_ID = $attachments[0]->ID;
						}
					}
					unset($image_flip);
					if(in_array('thumbnail_flip', $cms_section->options['item_data'])) {
						$attachments = get_posts(array(
							'posts_per_page'=> 1,
							'post_parent'	=> $item->ID,
							'post_type'		=> 'attachment',
							'post_mime_type'=> 'image',
							'orderby'		=> 'rand',
							'exclude'		=> $featured_image_ID
						));
						if($attachments) {
							$attachments = array_values($attachments);
							$image_flip = wp_get_attachment_image_src($attachments[0]->ID, CMSECT_THUMBNAIL_IMAGE_SIZE);
						}
						if($flip_image_from_cf = $cms_section->options['item_data_thumbnail_flip']) {
							if($custom_flip_image = get_post_meta($item->ID, $flip_image_from_cf, true))
								$image_flip = array($custom_flip_image);
						}
					}
				}
				
				$terms_HTML = '';
				if(in_array('taxonomies', $cms_section->options['item_data'])) {
					if(isset($cms_section->options['item_data_taxonomies_terms'])
					&& is_array($cms_section->options['item_data_taxonomies_terms'])) {
						$terms_HTML .= '<span class="cmsect-terms-by-taxonomy">';
						foreach($cms_section->options['item_data_taxonomies_terms'] as $terms_taxonomy) {
							if($terms = get_the_terms($item->ID, $terms_taxonomy)) {
								$terms_HTML .= '<span class="cmsect-taxonomy-'.$terms_taxonomy.'">';
								foreach($terms as $term) {
									$terms_HTML .= '<span class="cmsect-term term-'.$term->term_id.' term-slug-'.$term->slug.' term-taxonomy-'.$terms_taxonomy.'">'.$term->name.'</span>';
								}
								$terms_HTML .= '</span>';
							}
						}
						$terms_HTML .= '</span>';
					}
				}
				
				$href = $is_restricted ? ($cms_settings['restricted_href'] ? $cms_settings['restricted_href'] : 'javascript: void(0)') : get_permalink($item->ID);
				$href = apply_filters('cmsect_item_href', $href, $item);
				$extra_class = $is_restricted && $cms_section->options['restricted_class'] ? ' '.$cms_section->options['restricted_class'] : '';
				$excerpt = wp_trim_words(strip_tags(str_replace(']', '>', str_replace('[', '<', $item->post_content))), $cms_section->options['item_data_shortdescr_length'], '');
				$extra_props = apply_filters('cmsect_item_extra_props', '');
				
				$output_items_HTML .=
			'<a href="'.$href.'" class="cmsect-item'.$extra_class.'" data-cmsectitemid="'.$item->ID.'"'.(!empty($extra_props) ? ' '.$extra_props : '').'>'
				.((in_array('thumbnail', $cms_section->options['item_data']) && $image) ? '<span class="cmsect-item-thumb loading'.($image_flip && !$is_restricted ? ' has-flip-thumb' : '').'" data-cmsectar="'.$image[1].':'.$image[2].'">'
					.'<img src="'.$image[0].'" />'
					.((in_array('thumbnail_flip', $cms_section->options['item_data']) && $image_flip && !$is_restricted)
						? '<span class="flip-thumb" style="background-image: url('.$image_flip[0].')"></span>' : '')
				.'</span>' : '')
				.(in_array('post_title', $cms_section->options['item_data']) ? '<span class="cmsect-item-title">'.$item->post_title.'</span>' : '')
				.(in_array('shortdescr', $cms_section->options['item_data']) ? '<span class="cmsect-item-descr">'.$excerpt.'</span>' : '')
				.$terms_HTML
			.'</a>';
			
			}
		}
		
		if(defined('DOING_AJAX') && DOING_AJAX) {
			return $output_items_HTML;
		}
		
		$output_HTML .= $output_items_HTML
		.'</div><!-- .cmsect -->'
		.'</div><!-- .cmsect-container -->'
		.'<div class="cmsect-clearfix"></div>'
		.'<!-- END: Custom Masonry Section -->';
		
		return $output_HTML;
		
	} else return '';
}

add_action('wp_ajax_nopriv_cmsect_load_more', 'cmsect_ajax_load_more');
add_action('wp_ajax_cmsect_load_more', 'cmsect_ajax_load_more');
function cmsect_ajax_load_more() {
	if(isset($_POST['cmsect_id'])) {
		if($cmsect_more_items = cmsect_shortcode(array(
			'id'		=> $_POST['cmsect_id'],
			'offset'	=> intval($_POST['offset']),
			'exclude'	=> $_POST['exclude']
		))) {
			echo $cmsect_more_items;
			exit();
		}
	}
}

add_action('wp_enqueue_scripts', 'cmsect_wp_enqueue_scripts', 1000);
function cmsect_wp_enqueue_scripts() {
	global $cmsect_shortcodes_list, $post;
	$shortcode_found_in_content = false;
	foreach($cmsect_shortcodes_list as $cmsect_shortcode) {
		if(has_shortcode($post->post_content, $cmsect_shortcode))
			$shortcode_found_in_content = true;
	}
	/* allowing the css to load everywhere on the website, as there are area that need custom css, like login box, etc. */
	if($shortcode_found_in_content === true || 1) {
		wp_enqueue_style('cmsect-style', plugin_dir_url(__FILE__).'css/cmsect-style.php', false, false);
	}
}

add_action('wp_footer', 'cmsect_wp_footer');
function cmsect_wp_footer() {
	if(wp_script_is('jquery', 'queue')
	|| wp_script_is('jquery', 'done')
	|| wp_script_is('jquery', 'to_do')) {}
	else { ?>
	<script type="text/javascript" src="<?php bloginfo('url'); ?>/wp-includes/js/jquery/jquery.js"></script><?php }
	?><script type="text/javascript">
		var cmsect_sections,
			cmsect_ajax;
		jQuery(document).ready(function() {
			
			cmsect_ajax = '<?php echo admin_url('admin-ajax.php'); ?>';
			
			cmsect_sections = jQuery('.cmsect');
			
			if(jQuery('.cmsect').length > 0) {
				
				jQuery.getScript('<?php echo plugin_dir_url(__FILE__).'js/isotope.pkgd.min.js'; ?>', function() {
					
					if(typeof(cmsect_sections.isotope) != 'undefined') {
						cmsect_sections.each(function(i, cmsect_el) {
							cmsect_el = jQuery(cmsect_el);
							if(typeof(jQuery(cmsect_el).attr('data-restrictedclasshover')) != 'undefined') {
								jQuery(cmsect_el).data('restrictedclasshover', jQuery(cmsect_el).attr('data-restrictedclasshover'));
								jQuery(cmsect_el).removeAttr('data-restrictedclasshover');
							}
							var isotope_options = {
								itemSelector: '.cmsect-item',
								layoutMode: 'masonry', //'masonry'
								isInitLayout: false
							};
							jQuery(cmsect_el).find('.cmsect-item').not('.layed-out').each(function(j, item_el) { cmsect_adjust_item(item_el); });
							var $cmsect_section = cmsect_el.isotope(isotope_options);
							$cmsect_section.isotope('on', 'layoutComplete', function (isoInstance, laidOutItems) {
								// done after positioning is complete
								if($cmsect_section.find('.cmsect-item').length == 0) {
									$cmsect_section.addClass('no-items-to-show');
								}
							});
							$cmsect_section.isotope('layout');
							cmsect_el.data('cmsect', $cmsect_section);
						});
					} else if(typeof(console) != 'undefined') console.log('Failed to load custom masonry section.');
					
				});
				
				// Infinite scroll
				jQuery(window).scroll(function() {
					
					cmsect_sections.each(function(i, el) {
						
						if(!jQuery(el).hasClass('loading-more')
						&& !jQuery(el).hasClass('end-reached')) {
							
							var cmsect_section_end = jQuery(el).offset().top + jQuery(el).height();
							if(cmsect_section_end < (jQuery(window).scrollTop() + jQuery(window).height())
							&& cmsect_section_end > jQuery(window).scrollTop()) {
								jQuery(el).addClass('loading-more').after('<div class="cmsect-loading-more"></div>');
								var exclude = new Array();
								jQuery(el).find('.cmsect-item').each(function(k, itm) { exclude.push(jQuery(itm).data('cmsectitemid')); });
								jQuery.post(
									cmsect_ajax,
									{
										action:		'cmsect_load_more',
										cmsect_id:	jQuery(el).attr('id').toString().replace('cmsect-', ''),
										/*offset:		jQuery(el).find('.cmsect-item').length, Removed, as exclude will work better for all sort cases, esp. rand */
										exclude:	exclude.join(',')
									},
									function(response) {
										jQuery(el).removeClass('loading-more').next('.cmsect-loading-more').empty().remove();
										if(response == '0') {
											jQuery(el).addClass('end-reached').after('<div class="cmsect-thats-all"></div>');
										} else {
											var $new_items = jQuery(response),
												$cmsect_section = jQuery(el).data('cmsect');
											jQuery(el).append($new_items);
											jQuery(el).find('.cmsect-item').not('.layed-out').each(function(j, item_el) { cmsect_adjust_item(item_el); });
											jQuery(el).isotope('addItems', $new_items);
											jQuery(el).isotope('updateSortData', jQuery(el).children());
											jQuery(el).isotope();
										}
									}
								);
							}
							
						}
						
					});
					
				});
				
			}
		});
		
		function cmsect_adjust_item(item_el) {
			
			if(jQuery(item_el).find('.cmsect-item-thumb').length > 0) {
				jQuery(item_el).find('.cmsect-item-thumb').addClass('loading');
				var ar = jQuery(item_el).find('.cmsect-item-thumb').attr('data-cmsectar');
				jQuery(item_el).find('.cmsect-item-thumb').removeAttr('data-cmsectar').data('cmsectar', ar);
				if(typeof(ar) == 'string') {
					ar = ar.split(':');
					if(!isNaN(ar[0])) {
						var w = jQuery(item_el).find('.cmsect-item-thumb').width(),
							h = Math.ceil((jQuery(item_el).find('.cmsect-item-thumb').width() * ar[1]) / ar[0]);
						jQuery(item_el).find('.cmsect-item-thumb, .cmsect-item-thumb img').height(h);
					}
				}
				var img_obj = new Image();
				img_obj.src = jQuery(item_el).find('.cmsect-item-thumb img').attr('src');
				img_obj.onload = function() {
					jQuery(item_el).find('.cmsect-item-thumb').removeClass('loading');
				};
				if(jQuery(item_el).find('.cmsect-item-thumb .flip-thumb').length > 0) {
					var flip_img_obj = new Image();
					flip_img_obj.src = jQuery(item_el).find('.cmsect-item-thumb .flip-thumb').css('background-image').toString().replace(/^url\(["']?/, '').replace(/["']?\)$/, '');
					flip_img_obj.onload = function() {
						jQuery(item_el).find('.cmsect-item-thumb').addClass('flip-ready');
					};
				}
			}
			jQuery(item_el).addClass('layed-out').animate({ opacity: '1' }, 1000);
			
			jQuery(item_el).data('cmsectitemid', jQuery(item_el).attr('data-cmsectitemid'));
			jQuery(item_el).removeAttr('data-cmsectitemid');
			
			if(typeof(jQuery(item_el).closest('.cmsect').data('restrictedclasshover')) != 'undefined') {
				jQuery(item_el).hover(
					function() { jQuery(this).addClass(jQuery(this).closest('.cmsect').data('restrictedclasshover')); },
					function() { jQuery(this).removeClass(jQuery(this).closest('.cmsect').data('restrictedclasshover')); }
				);
			}
			
			if(typeof(window['cmsect_adjust_item_custom']) == 'function') window['cmsect_adjust_item_custom'](item_el);
			
		}
		
		var delay_func = (function(){
			var timer = 0;
			return function(callback, ms){
				clearTimeout (timer);
				timer = setTimeout(callback, ms);
			};
		})();
		
	</script><?php
}

?>