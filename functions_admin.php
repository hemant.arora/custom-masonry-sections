<?php
/**
 * @package Custom Masonry Sections
 * @version 0.1
 */

session_start();

define('CMSECT_ADMIN_NEW_SECTION_LINK', get_bloginfo('url').'/wp-admin/admin.php?page=cmsect_section');

add_action('admin_menu', 'cmsect_admin_menu');
function cmsect_admin_menu() {
	add_menu_page('Custom Masonry Sections', 'Masonry', 'manage_options', 'cmsect_sections', 'cmsect_sections_page_content', 'dashicons-schedule');
	add_submenu_page('cmsect_sections', 'Custom Masonry Sections', 'All Sections', 'manage_options', 'cmsect_sections', 'cmsect_sections_page_content');
	add_submenu_page('cmsect_sections', (isset($_GET['edit']) ? 'Edit' : 'Add New').' Custom Masonry Section', 'Add New', 'manage_options', 'cmsect_section', 'cmsect_section_page_content');
	add_submenu_page('cmsect_sections', 'Custom Masonry Settings', 'Settings', 'manage_options', 'cmsect_settings', 'cmsect_settings_page_content');
}

function cmsect_sections_page_content() { ?>
<div class="wrap">
	<h2>
		Custom Masonry Sections
		<a class="add-new-h2" href="<?php echo CMSECT_ADMIN_NEW_SECTION_LINK; ?>">Add New</a>
	</h2>
	<span class="">Simply use the shortcode(s) below on any page or post of your choice.</span>
	<p></p>
	
	<table class="wp-list-table widefat fixed posts">
		<thead>
			<tr>
				<th id="title" class="manage-column">Title</th>
				<th id="shortcode" class="manage-column">Shortcode</th>
				<th id="actions" class="manage-column" style="width: 150px">Actions</th>
			</tr>
		</thead>
		<tbody>
		<?php if($cmsects = cmsect_get_all()) {
			foreach($cmsects as $cmsect) { ?>
			<tr id="cmsect-<?php echo $cmsect->id; ?>">
				<td><?php echo $cmsect->title; ?></td>
				<td><code><?php echo cmsect_get_shortcode($cmsect->id); ?></code></td>
				<td>
					<a href="<?php echo bloginfo('url'); ?>/wp-admin/admin.php?page=cmsect_section&edit=<?php echo $cmsect->id; ?>" class="button">Edit</a>
					<a href="<?php echo bloginfo('url'); ?>/wp-admin/admin.php?page=cmsect_section&delete=<?php echo $cmsect->id; ?>" class="button">Delete</a>
				</td>
			</tr>
		<?php } } else { ?>
			<tr>
				<td colspan="3">
					<p style="padding: 50px; text-align: center">Please <a href="<?php echo CMSECT_ADMIN_NEW_SECTION_LINK; ?>">add a new custom masonry section</a> to start.</p>
				</td>
			</tr>
		<?php } ?>
		</tbody>
		<tfoot>
			<tr>
				<th id="title" class="manage-column">Title</th>
				<th id="shortcode" class="manage-column">Shortcode</th>
				<th id="actions" class="manage-column" style="width: 150px">Actions</th>
			</tr>
		</tfoot>
	</table>
</div><?php
}

function cmsect_section_page_content() {
	
	global
		$cmsect_custom_modules,
		$cmsect_custom_modules_admin_fields,
		$cms_section;
	
	$is_edit = (isset($_GET['edit']) && is_numeric($_GET['edit'])) ? $_GET['edit'] : false;
	
	if($is_edit !== false)
		$cms_section = cmsect_get($is_edit);
	else {
		$cms_section = new stdClass();
		$cms_section->title = '';
		$cms_section->options = array(
			'title'							=> $_POST['cms_section']['title'],
			'items_type'					=> $_POST['cms_section']['items_type'] ? $_POST['cms_section']['items_type'] : 'post',
			'items_type_custom'				=> $_POST['cms_section']['items_type_custom'] ? $_POST['cms_section']['items_type_custom'] : '',
			'items_per_page'				=> $_POST['cms_section']['items_per_page'] ? $_POST['cms_section']['items_per_page'] : 10,
			'sort_items'					=> $_POST['cms_section']['sort_items'] ? $_POST['cms_section']['sort_items'] : 'date_desc',
			'filter_items'					=> $_POST['cms_section']['filter_items'] ? $_POST['cms_section']['filter_items'] : '',
			'filter_by'						=> is_array($_POST['cms_section']['filter_by']['category']) ? $_POST['cms_section']['filter_by']['category'] : array('category' => array()),
			'item_data'						=> is_array($_POST['cms_section']['item_data']) ? $_POST['cms_section']['item_data'] : ($_POST['cms_section']['item_data'] ? array($_POST['cms_section']['item_data']) : array()),
			'item_data_thumbnail_flip'		=> $_POST['cms_section']['item_data_thumbnail_flip'],
			'item_data_shortdescr_length'	=> $_POST['cms_section']['item_data_shortdescr_length'],
			'item_data_taxonomies_terms'	=> $_POST['cms_section']['item_data_taxonomies_terms']
		);
	}
	$cms_section->options['item_data'] = is_array($cms_section->options['item_data']) ? $cms_section->options['item_data'] : array();
	$cms_section->options['item_data_taxonomies_terms'] = is_array($cms_section->options['item_data_taxonomies_terms']) ? $cms_section->options['item_data_taxonomies_terms'] : array($cms_section->options['item_data_taxonomies_terms']);
	$cms_section->options['filter_by']['category'] = is_array($cms_section->options['filter_by']['category']) ? $cms_section->options['filter_by']['category'] : array();
	$args=array(
		'public'                => true,
		'exclude_from_search'   => false,
		'_builtin'              => false
	);
	$post_types = get_post_types($args, 'objects', 'and');
	
	$taxonomies = cmsect_fetch_object_taxonomies($cms_section->options['items_type'] == 'custom' ? $cms_section->options['items_type_custom'] : $cms_section->options['items_type']);
	
	cmsect_admin_styles(); ?>
<div class="wrap">
	<h2>
		<?php echo $is_edit === false ? 'Add' : 'Edit'; ?> Custom Masonry Section
		<?php if($is_edit !== false) { ?><a class="add-new-h2" href="<?php echo CMSECT_ADMIN_NEW_SECTION_LINK; ?>">Add New</a><?php } ?>
	</h2>
	<p></p>
	
	<form class="frm-cms-section cms-grid cms-form" method="post" action="">
		<input type="hidden" name="cmsect_save_action" value="<?php echo ($is_edit !== false) ? 'edit' : 'add'; ?>" />
		<?php if($is_edit !== false) { ?><input type="hidden" name="cms_section[id]" value="<?php echo $is_edit; ?>" /><?php } ?>
		
		<div class="field">
			<label class="title" for="cms-section-title">Title</label>
			<span class="descr mrgn-b-10">(This title is meant just for identification and is not displayed on the website)</span>
			<input type="text" id="cms-section-title" value="<?php echo $cms_section->title; ?>" size="30" name="cms_section[title]" />
		</div>
		
		<div class="field">
			<span class="title">Items to Display</span>
			<span class="descr mrgn-b-10">Select the type of items that you wish to display</span>
			<label><input type="radio" name="cms_section[items_type]" value="post"<?php echo $cms_section->options['items_type'] == 'post' ? ' checked=""' : ''; ?> /> WP Posts</label>
			<?php
				if($post_types) { ?>
			<label><input type="radio" name="cms_section[items_type]" value="custom"<?php echo $cms_section->options['items_type'] == 'custom' ? ' checked=""' : ''; ?> /> Custom Post Type</label>
			
			<select name="cms_section[items_type_custom]" class="mrgn-t-15"<?php echo $cms_section->options['items_type'] == 'custom' ? '' : ' disabled=""'; ?>>
			<?php	foreach($post_types as $post_type_slug => $post_type_options) { ?>
				<option value="<?php echo $post_type_slug; ?>"<?php echo $cms_section->options['items_type_custom'] == $post_type_slug ? ' selected=""' : ''; ?>><?php echo $post_type_options->labels->name; ?></option>
			<?php 	} ?>
			</select>
			<?php } ?>
		</div>
		
		<div class="field">
			<label class="title" for="cms_section_filter_items"><input type="checkbox" name="cms_section[filter_items]" id="cms_section_filter_items" value="Y"<?php echo $cms_section->options['filter_items'] == 'Y' ? ' checked=""' : ''; ?> /> Filter Items...</label>
			<span class="descr mrgn-b-10">Please check this option to filter and select specific (group of) items to display via the options below (check to view options).</span>
			
			<div class="field-option mrgn-b-10 hidden"<?php echo $cms_section->options['filter_items'] == 'Y' ? ' style="display: block"' : ''; ?>>
				<div class="sub-option mrgn-l-20"<?php echo $cms_section->options['filter_items'] == 'Y' ? ' style="display: block"' : ''; ?>>
					<label>Show items from specific category/term(s)</label>
					<span class="descr">Select the category/term(s) to display the items under them. Hold down the Ctrl/Cmd key to select multiple categories/terms.</span>
					<select name="cms_section[filter_by][category][]" multiple=""<?php echo ($cms_section->options['filter_items'] == 'Y') ? '' : ' disabled=""'; ?>>
					<?php if($taxonomies) {
						$terms_found = 0;
						foreach($taxonomies as $taxonomy_slug => $taxonomy_data) {
							if($taxonomy_data->show_ui === true) {
								if($terms = get_terms($taxonomy_slug, array('hierarchical' => false))) { ?>
						<optgroup label="<?php echo $taxonomy_data->labels->name; ?>"><?php
									foreach($terms as $term) { $terms_found++; ?>
						<option value="<?php echo $term->term_id; ?>"<?php echo in_array($term->term_id, $cms_section->options['filter_by']['category']) ? ' selected=""' : ''; ?>><?php echo $term->name; ?></option>
					<?php } } ?>
						</optgroup><?php
					} } if($terms_found == 0) { ?>
						<option value="" disabled="">No terms found</option><option value="" disabled="">for this post type</option>
					<?php } } else { ?>
						<option value="" disabled="">No terms found</option><option value="" disabled="">for this post type</option><?php } ?>
					</select>
				</div>
			</div>
			
			<div class="field-option mrgn-b-10 hidden"<?php echo $cms_section->options['filter_items'] == 'Y' ? ' style="display: block"' : ''; ?>>
				<div class="sub-option mrgn-l-20"<?php echo $cms_section->options['filter_items'] == 'Y' ? ' style="display: block"' : ''; ?>>
					<label>Exclude specific items</label>
					<span class="descr">List the IDs of items that you wish to exclude. Seperate multiple IDs with comma.</span>
					<input type="text" name="cms_section[filter_by][exclude_ids]" value="<?php echo $cms_section->options['filter_by']['exclude_ids']; ?>" <?php echo ($cms_section->options['filter_items'] == 'Y') ? '' : ' disabled=""'; ?> />
				</div>
			</div>
		</div>
		
		<div class="field">
			<label class="title" for="cms_section_sort_items">Sort Items</label>
			<span class="descr mrgn-b-10">Please check this option to order items.</span>
			<select name="cms_section[sort_items]" id="cms_section_sort_items">
				<option value="date_desc"<?php echo 'date_desc' == $cms_section->options['sort_items'] ? ' selected=""' : ''; ?>>Date Created (Descending)</option>
				<option value="date_asc"<?php echo 'date_asc' == $cms_section->options['sort_items'] ? ' selected=""' : ''; ?>>Date Created (Ascending)</option>
				<option value="mod_desc"<?php echo 'mod_desc' == $cms_section->options['sort_items'] ? ' selected=""' : ''; ?>>Date Modified (Descending)</option>
				<option value="mod_asc"<?php echo 'mod_asc' == $cms_section->options['sort_items'] ? ' selected=""' : ''; ?>>Date Modified (Ascending)</option>
				<option value="title_asc"<?php echo 'title_asc' == $cms_section->options['sort_items'] ? ' selected=""' : ''; ?>>Title (Ascending)</option>
				<option value="comm_desc"<?php echo 'comm_desc' == $cms_section->options['sort_items'] ? ' selected=""' : ''; ?>>Comment Count (Descending)</option>
				<option value="comm_asc"<?php echo 'comm_asc' == $cms_section->options['sort_items'] ? ' selected=""' : ''; ?>>Comment Count (Ascending)</option>
				<option value="rand"<?php echo 'rand' == $cms_section->options['sort_items'] ? ' selected=""' : ''; ?>>Random Order</option>
			</select>
		</div>
		
		<div class="field">
			<label class="title">Item Data to Display</label>
			<span class="descr mrgn-b-10">Please choose below the information that needs to be displayed on every brick of item.</span>
			
			<div class="field-option mrgn-b-10">
				<label><input type="checkbox" name="cms_section[item_data][]" value="post_title"<?php echo in_array('post_title', $cms_section->options['item_data']) ? ' checked=""' : ''; ?> /> Item Title</label>
			</div>
			
			<div class="field-option mrgn-b-10">
				<label><input type="checkbox" name="cms_section[item_data][]" value="thumbnail"<?php echo in_array('thumbnail', $cms_section->options['item_data']) ? ' checked=""' : ''; ?> /> Thumbnail</label>
			</div>
			
			<div class="field-option mrgn-b-10">
				<label><input type="checkbox" name="cms_section[item_data][]" value="thumbnail_flip"<?php echo in_array('thumbnail_flip', $cms_section->options['item_data']) ? ' checked=""' : ''; ?>
					<?php echo in_array('thumbnail', $cms_section->options['item_data']) ? '' : ' disabled=""'; ?> /> Thumbnail Flip</label>
				<div class="sub-option mrgn-l-20"<?php echo in_array('thumbnail_flip', $cms_section->options['item_data']) ? ' style="display: block"' : ''; ?>>
					Custom field value to be used for flip image
					<span class="descr">If left blank, a random attachment image (if exists) other than the featured image is used.</span>
					<input type="text" name="cms_section[item_data_thumbnail_flip]" value="<?php echo $cms_section->options['item_data_thumbnail_flip']; ?>" />
				</div>
			</div>
			
			<div class="field-option mrgn-b-10">
				<label><input type="checkbox" name="cms_section[item_data][]" value="shortdescr"<?php echo in_array('shortdescr', $cms_section->options['item_data']) ? ' checked=""' : ''; ?> /> Short Description</label>
				<div class="sub-option mrgn-l-20"<?php echo in_array('shortdescr', $cms_section->options['item_data']) ? ' style="display: block"' : ''; ?>>
					Maximum number of words
					<select name="cms_section[item_data_shortdescr_length]"<?php echo in_array('shortdescr', $cms_section->options['item_data']) ? '' : ' disabled=""'; ?>>
					<?php for($x = 10; $x <= 50; $x += 5) { ?>
						<option value="<?php echo $x; ?>"<?php echo $cms_section->options['item_data_shortdescr_length'] == $x ? ' selected=""' : ''; ?>><?php echo $x; ?> words</option>
					<?php } ?>
					</select>
				</div>
			</div>
			
			<div class="field-option mrgn-b-10">
				<label><input type="checkbox" name="cms_section[item_data][]" value="taxonomies"<?php echo in_array('taxonomies', $cms_section->options['item_data']) ? ' checked=""' : ''; ?> /> Taxonomies</label>
				<div class="sub-option mrgn-l-20"<?php echo in_array('taxonomies', $cms_section->options['item_data']) ? ' style="display: block"' : ''; ?>>
					Select the taxonomy terms to display
					<span class="descr">Hold down the Ctrl/Cmd key to select multiple taxonomies</span>
					<select name="cms_section[item_data_taxonomies_terms][]" multiple=""<?php echo (in_array('taxonomies', $cms_section->options['item_data']) && $taxonomies) ? '' : ' disabled=""'; ?>>
					<?php if($taxonomies) {
						foreach($taxonomies as $taxonomy_slug => $taxonomy_data) {
							if($taxonomy_data->show_ui === true) { ?>
						<option value="<?php echo $taxonomy_slug; ?>"<?php echo in_array($taxonomy_slug, $cms_section->options['item_data_taxonomies_terms']) ? ' selected=""' : ''; ?>><?php echo $taxonomy_data->labels->name; ?></option>
					<?php } } } else { ?>
						<option value="" disabled="">No taxonomies found</option><option value="" disabled="">for this post type</option>
					<?php } ?>
					</select>
				</div>
			</div>
			
		</div>
		
		<div class="field">
			<label class="title" for="items-per-page">Items per Page</label>
			<span class="descr mrgn-b-10">Items to display per page. In case of infinite scroll, this is the amount of items that will be fetched each time.</span>
			<select name="cms_section[items_per_page]" id="items-per-page">
			<?php for($x = 5; $x <= 50; $x += 5) { ?>
				<option value="<?php echo $x; ?>"<?php echo $cms_section->options['items_per_page'] == $x ? ' selected=""' : ''; ?>><?php echo $x; ?> items</option>
			<?php } /* ?>
				<option value="custom"<?php echo $cms_section->options['items_per_page'] == 'custom' ? ' selected=""' : ''; ?>>Custom...</option>*/ ?>
			</select>
			<?php /*<input type="text" name="cms_section[items_per_page_custom]" value="<?php echo $cms_section->options['items_per_page_custom']; ?>"
				style="width: 40px<?php echo $cms_section->options['items_per_page'] == 'custom' ? '' : '; display: none'; ?>" />*/ ?>
		</div>
		
		<?php /* TO BE DEVELOPED LATER
		<div class="field diablo">
			<label class="title" for="cms_section_display_filters"><input type="checkbox" name="cms_section[display_filters]" id="cms_section_display_filters" value="Y"<?php echo $cms_section->options['display_filters'] == 'Y' ? ' checked=""' : ''; ?> /> Display filter buttons above masonry...</label>
			<span class="descr mrgn-b-10">Please check this option if you want to display filter buttons above the items masonry. These buttons allow you to either view all the items or the items under a specific category/term. These terms can be chosen when you select this option.</span>
		</div>
		*/ ?>
		
		<?php if($is_edit) { ?>
		<div class="field">
			<label class="title" for="cms_section_custom_css">Custom CSS</label>
			<span class="descr mrgn-b-10">Please add your custom style for the masonry section below.
				<br /><strong>NOTE:</strong><br />It is recommended to prefix every CSS style definition with <code>#cmsect-<?php echo $cms_section->id; ?></code> to prevent clashes between multiple masonry sections on same page (if any).
				<br /><strong>For example:</strong><br /><code>#cmsect-1.cmsect .cmsect-item { width: 150px }</code></span>
			<div class="field-option mrgn-b-10">
				<textarea name="cms_section[custom_css]" id="cms_section_custom_css" style="height: 150px;"><?php echo stripslashes($cms_section->options['custom_css']); ?></textarea>
			</div>
		</div>
		<?php } ?>
		
		<?php
		foreach($cmsect_custom_modules as $cmsect_custom_module) {
			if(isset($cmsect_custom_modules_admin_fields[$cmsect_custom_module])) {
				if(function_exists($cmsect_custom_modules_admin_fields[$cmsect_custom_module]))
					call_user_func($cmsect_custom_modules_admin_fields[$cmsect_custom_module]);
			}
		} ?>
		
		<div class="field">
			<p class="submit">
				<input type="submit" class="button-primary" value="Save" />
				<?php if($is_edit) { ?>
				<input type="button" class="button-primary save-n-continue-editing" value="Save & Continue Editing" /><?php } ?>
				<a href="<?php echo bloginfo('url'); ?>/wp-admin/admin.php?page=cmsect_sections" class="button">Cancel</a>
			</p>
		</div>
		
	</form>
	
</div><?php
	cmsect_admin_scripts();
}

function cmsect_settings_page_content() {
	global
		$cmsect_custom_modules,
		$cmsect_custom_modules_global_admin_fields,
		$cms_settings;
	
	$cms_settings = cmsect_get_settings();
	
	cmsect_admin_styles(); ?>
<div class="wrap">
	<h2>
		Custom Masonry Sections &mdash; Global Settings
	</h2>
	<p></p>
	
	<form class="frm-cms-settings cms-grid cms-form" method="post" action="">
		<input type="hidden" name="cmsect_settings_action" value="save" />
		
		<div class="field">
			<label class="title" for="cms_settings_custom_css">Global Custom CSS</label>
			<span class="descr mrgn-b-10">Please add custom style for the masonry sections below.
				<br /><strong>NOTE:</strong> These styles are meant to apply to all masonry sections.</span>
			<div class="field-option mrgn-b-10">
				<textarea name="cms_settings[custom_css]" id="cms_settings_custom_css" style="height: 150px;"><?php echo stripslashes($cms_settings['custom_css']); ?></textarea>
			</div>
		</div>
		
		<?php
		foreach($cmsect_custom_modules as $cmsect_custom_module) {
			if(isset($cmsect_custom_modules_global_admin_fields[$cmsect_custom_module])) {
				if(function_exists($cmsect_custom_modules_global_admin_fields[$cmsect_custom_module]))
					call_user_func($cmsect_custom_modules_global_admin_fields[$cmsect_custom_module]);
			}
		} ?>
		
		<div class="field">
			<p class="submit">
				<input type="submit" class="button-primary" value="Save Changes" />
			</p>
		</div>
		
	</form><?php
	cmsect_admin_scripts();
}

function cmsect_admin_styles() {
?><style>
.cms-form { max-width: 100%; width: 500px; }

.cms-grid .hidden { display: none }

.cms-grid .mrgn-b-5 { margin-bottom: 5px }
.cms-grid .mrgn-b-10 { margin-bottom: 10px }
.cms-grid .mrgn-b-15 { margin-bottom: 15px }
.cms-grid .mrgn-b-20 { margin-bottom: 20px }

.cms-grid .mrgn-t-5 { margin-top: 5px }
.cms-grid .mrgn-t-10 { margin-top: 10px }
.cms-grid .mrgn-t-15 { margin-top: 15px }
.cms-grid .mrgn-t-20 { margin-top: 20px }

.cms-grid .mrgn-l-5 { margin-left: 5px }
.cms-grid .mrgn-l-10 { margin-left: 10px }
.cms-grid .mrgn-l-15 { margin-left: 15px }
.cms-grid .mrgn-l-20 { margin-left: 20px }

.cms-grid .mrgn-r-5 { margin-right: 5px }
.cms-grid .mrgn-r-10 { margin-right: 10px }
.cms-grid .mrgn-r-15 { margin-right: 15px }
.cms-grid .mrgn-r-20 { margin-right: 20px }

.cms-form .field { display: block; margin-bottom: 35px }
.cms-form .field label { display: block; }
.cms-form .field .title { display: block; font-size: 16px; font-weight: bold }
.cms-form .field .descr { color: #999; display: block; }
.cms-form .field input[type="text"],
.cms-form .field textarea { border-radius: 2px; padding: 6px 10px; width: 100%; }
.cms-form .field select { min-width: 150px; }
.cms-form .field .sub-option { border-left: 3px solid rgba(0, 0, 0, 0.1); display: none; margin: 5px 0 10px 23px; padding: 0 0 0 10px }
.cms-form .field .sub-option input[type="text"],
.cms-form .field .sub-option select { max-width: 80% !important; width: 200px !important }
.cms-form .field .sub-option select[multiple] { min-height: 100px }
.cms-form .field .sub-option input,
.cms-form .field .sub-option select { display: block; width: 100% }

.diablo { position: relative }
.diablo:before { background-color: rgba(241,241,241,0.7); content: ''; display: block; height: 100%; left: 0; position: absolute; top: 0; width: 100% }
</style><?php
}

function cmsect_admin_scripts() {
?><script type="text/javascript">
	
	var ajaxurl;
	
	jQuery(document).ready(function() {
		
		if(typeof(ajaxurl) == 'undefined')
			ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
		
		jQuery('.frm-cms-section .save-n-continue-editing').click(function(e) {
			e.preventDefault();
			jQuery(this).closest('form').append('<input type="hidden" name="cmsect_continue_editing" value="1" />');
			jQuery(this).closest('form').submit();
		});
		
		jQuery('.frm-cms-section').submit(function() {
			jQuery(this).find('[type="submit"], [type="button"]').attr('disabled', true);
		});
		
		jQuery('.frm-cms-section input[name="cms_section[items_type]"]').click(function() {
			var items_type = jQuery(this).val();
			var items_type_custom_selector = jQuery(this).closest('form').find('select[name="cms_section[items_type_custom]"]');
			jQuery(items_type_custom_selector).attr('disabled', (items_type == 'custom') ? false : true);
			if(items_type == 'custom') {
				jQuery(items_type_custom_selector).val(jQuery(items_type_custom_selector).find('option:first-child').attr('value'));
				cmsect_ajax_fetch_object_taxonomies(jQuery(items_type_custom_selector).val());
			} else {
				cmsect_ajax_fetch_object_taxonomies(jQuery(this).val());
			}
		});
		
		jQuery('.frm-cms-section select[name="cms_section[items_type_custom]"]').change(function() {
			cmsect_ajax_fetch_object_taxonomies(jQuery(this).val());
		});
		
		jQuery('.frm-cms-section input[name="cms_section[item_data][]"]').click(function() {
			if(jQuery(this).val() == 'thumbnail') {
				var thumbnail_flip_checkbox = jQuery('.frm-cms-section input[name="cms_section[item_data][]"][value="thumbnail_flip"]');
				jQuery(thumbnail_flip_checkbox).attr('disabled', !jQuery(this).is(':checked'));
				if(jQuery(this).is(':checked')) {
					jQuery(thumbnail_flip_checkbox).closest('.field-option').find('.sub-option').slideDown('fast');
					jQuery(thumbnail_flip_checkbox).closest('.field-option').find('.sub-option').find('input, select').attr('disabled', false);
				} else {
					jQuery(thumbnail_flip_checkbox).closest('.field-option').find('.sub-option').slideUp('fast');
					jQuery(thumbnail_flip_checkbox).closest('.field-option').find('.sub-option').find('input, select').attr('disabled', true);
				}
			}
			if(jQuery(this).is(':checked')) {
				jQuery(this).closest('.field-option').find('.sub-option').slideDown('fast');
				jQuery(this).closest('.field-option').find('.sub-option').find('input, select').attr('disabled', false).focus();
			} else {
				jQuery(this).closest('.field-option').find('.sub-option').slideUp('fast');
				jQuery(this).closest('.field-option').find('.sub-option').find('input, select').attr('disabled', true);
			}
		});
		
		jQuery('.frm-cms-section').find('input[name="cms_section[filter_items]"], input[name="cms_section[is_restricted]"]').click(function() {
			var self = jQuery(this);
			if(jQuery(self).is(':checked')) {
				jQuery(self).closest('.field').find('.field-option').show();
				jQuery(self).closest('.field').find('.sub-option').slideDown('fast');
				jQuery(self).closest('.field').find('.field-option').find('input, select').attr('disabled', false);
			} else {
				jQuery(self).closest('.field').find('.sub-option').slideUp('fast', function() { jQuery(self).closest('.field').find('.field-option').hide(); });
				jQuery(self).closest('.field').find('.field-option').find('input, select').attr('disabled', true);
			}
		});
		
	});
	
	function cmsect_ajax_fetch_object_taxonomies(object_slug) {
		
		var tax_selectbox = jQuery('.frm-cms-section select[name="cms_section[item_data_taxonomies_terms][]"]'),
			filter_by_terms_selectbox = jQuery('.frm-cms-section select[name="cms_section[filter_by][category][]"]');
		jQuery(tax_selectbox).attr('disabled', true);
		jQuery(filter_by_terms_selectbox).attr('disabled', true);
		jQuery.post(
			ajaxurl,
			{
				action: 		'cmsect_fetch_object_taxonomies',
				object_slug:	object_slug
			},
			function(response) {
				if(!isNaN(response)
				|| !isJson(response)) {
					jQuery(tax_selectbox).html('<option value="" disabled="">No taxonomies found</option><option value="" disabled="">for this post type</option>');
					jQuery(filter_by_terms_selectbox).html('<option value="" disabled="">No terms found</option><option value="" disabled="">for this post type</option>');
				} else {
					jQuery(tax_selectbox).attr('disabled', false).html('');
					var res = JSON.parse(response),
						tax_slugs = new Array();
					for(property in res) {
						tax_slugs.push(property);
						jQuery(tax_selectbox).append('<option value="'+property+'">'+res[property].labels.name+'</option>');
					}
					jQuery.post(
						ajaxurl,
						{
							action: 		'cmsect_fetch_taxonomies_terms',
							taxonomies:		tax_slugs.join(',')
						},
						function(response_terms) {
							if(!isNaN(response_terms)
							|| !isJson(response_terms)) {
								jQuery(filter_by_terms_selectbox).html('<option value="" disabled="">No terms found</option><option value="" disabled="">for this post type</option>');
							} else {
								jQuery(filter_by_terms_selectbox).attr('disabled', jQuery('input[name="cms_section[filter_items]"]').is(':checked') ? false : true).html('');
								var res_terms = JSON.parse(response_terms);
								for(property in res_terms) {
									var tax_terms_HTML = '<optgroup label="'+jQuery('select[name="cms_section[item_data_taxonomies_terms][]"] option[value="'+property+'"]').html()+'">';
									for(key in res_terms[property])
										tax_terms_HTML += '<option value="'+res_terms[property][key].term_id+'">'+res_terms[property][key].name+'</option>'
									tax_terms_HTML += '</optgroup>'
									jQuery(filter_by_terms_selectbox).append(tax_terms_HTML);
								}
							}
						}
					);
				}
			}
		);
		
	}
	
	function isJson(str) {
		try {
			JSON.parse(str);
		} catch (e) {
			return false;
		}
		return true;
	}
	
</script><?php
}

add_action('wp_loaded', 'cmsect_save_action_handler');
function cmsect_save_action_handler() {
	if(isset($_POST['cmsect_save_action'])
	&& current_user_can('manage_options')) {
		global $wpdb;
		
		$cms_section = $_POST['cms_section'];
		$is_valid = true;
		
		if(trim($cms_section['title']) == '') {
			$is_valid = false;
			$_SESSION['CMSECT_SAVE_ACTION_MSG'] = 'Please enter a title for identification of this masonry section.';
		} elseif(trim($cms_section['items_type']) == '') {
			$is_valid = false;
			$_SESSION['CMSECT_SAVE_ACTION_MSG'] = 'Please select the type of items to display.';
		} elseif(count($cms_section['item_data']) == 0) {
			$is_valid = false;
			$_SESSION['CMSECT_SAVE_ACTION_MSG'] = 'Please select one or more item data to display.';
		}
		
		$cms_title = trim($cms_section['title']);
		$cms_data = $cms_section;
		unset($cms_data['id'], $cms_data['title']);
		
		if($is_valid === true) {
			if(isset($cms_section['id'])
			&& is_numeric($cms_section['id'])
			&& $cms_section['id'] > 0
			&& $_POST['cmsect_save_action'] == 'edit'
			&& $cms_section_saved = $wpdb->get_row($wpdb->prepare("SELECT id FROM {$wpdb->prefix}cms_sections WHERE id = %d LIMIT 1", $cms_section['id']))) {
				$saved = $wpdb->query($wpdb->prepare("
				UPDATE {$wpdb->prefix}cms_sections
				SET
					title = %s,
					options = %s
				WHERE
					id = %d
				LIMIT 1",
				$cms_title,
				serialize($cms_data),
				$cms_section['id']));
				if($saved || 1) {
					$_SESSION['CMSECT_SAVE_ACTION_STATUS'] = true;
					$_SESSION['CMSECT_SAVE_ACTION_MSG'] = 'Your masonry section has been saved successfully.';
				} /*else {
					$_SESSION['CMSECT_SAVE_ACTION_STATUS'] = false;
					$_SESSION['CMSECT_SAVE_ACTION_MSG'] = 'An error has occured when trying to save the masonry section.';
				}*/
			} elseif($_POST['cmsect_save_action'] == 'add') {
				$created = $wpdb->query($wpdb->prepare("
				INSERT INTO {$wpdb->prefix}cms_sections
				(title, options)
				VALUES
				(%s, %s)",
				$cms_title,
				serialize($cms_data)));
				if($created) {
					$_SESSION['CMSECT_SAVE_ACTION_STATUS'] = true;
					$_SESSION['CMSECT_SAVE_ACTION_MSG'] = 'Your masonry section has been created successfully. Now, you may use the generated shortcode on the post/page of your choice to display the masonry.';
				} else {
					$_SESSION['CMSECT_SAVE_ACTION_STATUS'] = false;
					$_SESSION['CMSECT_SAVE_ACTION_MSG'] = 'An error has occured when trying to create the masonry section.';
				}
			}
		} else {
			$_SESSION['CMSECT_SAVE_ACTION_STATUS'] = false;
		}
		
	}
	
	if(isset($_POST['cmsect_settings_action'])
	&& $_POST['cmsect_settings_action'] == 'save'
	&& current_user_can('manage_options')) {
		
		if(isset($_POST['cms_settings'])
		&& is_array($_POST['cms_settings'])) {
			update_option('cmsect_settings', $_POST['cms_settings']);
			$_SESSION['CMSECT_SETTINGS_SAVE_ACTION_MSG'] = 'The custom masonry sections settings have been saved.';
		}
		
	}
	
}

function cmsect_get_settings() {
	return get_option('cmsect_settings', array());
}

add_action('admin_notices', 'cmsect_admin_notices');
function cmsect_admin_notices() {
	
	if(isset($_SESSION['CMSECT_SAVE_ACTION_STATUS'])) { ?>
	<div class="<?php echo $_SESSION['CMSECT_SAVE_ACTION_STATUS'] === true ? 'updated' : 'error'; ?>">
		<p><?php echo $_SESSION['CMSECT_SAVE_ACTION_MSG']; ?></p>
	</div>
	<?php
		unset($_SESSION['CMSECT_SAVE_ACTION_STATUS'], $_SESSION['CMSECT_SAVE_ACTION_MSG']);
	}
	
	if(isset($_SESSION['CMSECT_SETTINGS_SAVE_ACTION_MSG'])) { ?>
	<div class="updated">
		<p><?php echo $_SESSION['CMSECT_SETTINGS_SAVE_ACTION_MSG']; ?></p>
	</div>
	<?php unset($_SESSION['CMSECT_SETTINGS_SAVE_ACTION_MSG']);
	}
	
}

add_action('current_screen', function() {
	$screen = get_current_screen();
	if($_SESSION['CMSECT_SAVE_ACTION_STATUS'] === true
	&& !(strpos($screen->base, 'cmsect_sections') > -1)
	&& !isset($_POST['cmsect_continue_editing'])) {
		wp_redirect(get_option('siteurl').'/wp-admin/admin.php?page=cmsect_sections');
		exit();
	}
});










/**
 * Ajax events
 * -----------
 **/
add_action('wp_ajax_cmsect_fetch_object_taxonomies', 'cmsect_ajax_fetch_object_taxonomies');
function cmsect_ajax_fetch_object_taxonomies() {
	if(current_user_can('manage_options')
	&& isset($_POST['object_slug'])) {
		$taxos = cmsect_fetch_object_taxonomies($_POST['object_slug']);
		$taxonomies = array();
		foreach($taxos as $taxo_slug => $taxo_data) {
			if($taxo_data->show_ui === true)
				$taxonomies[$taxo_slug] = $taxo_data;
		}
		if(count($taxonomies) == 0) die('0');
		echo json_encode($taxonomies);
		exit();
	}
}

function cmsect_fetch_object_taxonomies($object_slug) {
	return get_object_taxonomies($object_slug, 'objects');
}

add_action('wp_ajax_cmsect_fetch_taxonomies_terms', 'cmsect_ajax_fetch_taxonomies_terms');
function cmsect_ajax_fetch_taxonomies_terms() {
	if(current_user_can('manage_options')
	&& isset($_POST['taxonomies'])) {
		$terms_by_taxos = cmsect_fetch_taxonomies_terms(is_array($_POST['taxonomies']) ? $_POST['taxonomies'] : explode(',', $_POST['taxonomies']));
		if(count($terms_by_taxos) == 0) die('0');
		echo json_encode($terms_by_taxos);
		exit();
	}
}

function cmsect_fetch_taxonomies_terms($taxonomies) {
	$terms_by_taxonomies = array();
	if(!is_array($taxonomies)) return $terms_by_taxonomies;
	foreach($taxonomies as $taxonomy_slug) {
		$terms_by_taxonomies[$taxonomy_slug] = get_terms($taxonomy_slug);
		if(count($terms_by_taxonomies[$taxonomy_slug]) == 0) unset($terms_by_taxonomies[$taxonomy_slug]);
	}
	return $terms_by_taxonomies;
}












/**
 * Get all CMS
 * -----------
 **/
function cmsect_get_all() {
	global $wpdb;
	$cms_sections = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}cms_sections WHERE enabled = 'Y' ORDER BY title ASC");
	if($cms_sections) {
		for($x = 0; $x < count($cms_sections); $x++)
			$cms_sections[$x]->options = unserialize($cms_sections[$x]->options);
		return $cms_sections;
	}
	return false;
}

/**
 * Get a specific CMS
 * ------------------
 **/
function cmsect_get($cms_id) {
	global $wpdb;
	if($cms_section = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$wpdb->prefix}cms_sections WHERE id = %d LIMIT 1", $cms_id))) {
		$cms_section->options = unserialize($cms_section->options);
		return $cms_section;
	}
	return false;
}

/**
 * Setup CMS
 * ---------
 **/
function cmsect_setup() {
	global $wpdb;
	$structure = 
"CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}cms_sections` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `options` longtext NOT NULL,
  `enabled` enum('Y','N') NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`)
);";
	$wpdb->query($structure);
	
	if(function_exists('cmsect_cust_setup'))
		cmsect_cust_setup();
}




/**
 * Debug / Misc
 * ------------
 **/
function cmsect_pre_print($data) {
	echo '<pre>'.print_r($data, true).'</pre>';
}

?>