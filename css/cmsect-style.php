<?php
/**
 * @package Custom Masonry Sections
 * @version 0.1
 */
	
	define('WP_USE_THEMES', false);
	require_once '../../../../wp-load.php';
	
	header('Content-Type: text/css');
	
?>.cmsect-spinner,
.cmsect-item .cmsect-item-thumb.loading:after,
*[id^="cmsect"] .loading[type="submit"]:after,
*[class^="cmsect"] .loading[type="submit"]:after {
	   -moz-animation: cmsect-rotation .6s infinite linear;
	-webkit-animation: cmsect-rotation .6s infinite linear;
	     -o-animation: cmsect-rotation .6s infinite linear;
	        animation: cmsect-rotation .6s infinite linear;
	border-color: rgba(0, 0, 0, 0.2) rgba(0, 0, 0, 0.05) rgba(0, 0, 0, 0.05);
	border-radius: 100%;
	border-style: solid;
	border-width: 6px;
	height: 30px;
	margin: 94px auto 0;
	position: relative;
	width: 30px;
}
@-moz-keyframes cmsect-rotation {
	from { -moz-transform: rotate(0deg); }
	to { -moz-transform: rotate(359deg); }
}
@-webkit-keyframes cmsect-rotation {
	from { -webkit-transform: rotate(0deg); }
	to { -webkit-transform: rotate(359deg); }
}
@-o-keyframes cmsect-rotation {
	from { -o-transform: rotate(0deg); }
	to { -o-transform: rotate(359deg); }
}
@keyframes cmsect-rotation {
	from { transform: rotate(0deg); }
	to { transform: rotate(359deg); }
}

.cmsect,
.cmsect * {
	   -moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
	     -o-box-sizing: border-box;
	    -ms-box-sizing: border-box;
	        box-sizing: border-box;
	position: relative;
	outline: none !important
}

.cmsect-clearfix { clear: both }
.clearfix:after,
.cmsect-clearfix:before,
.cmsect-clearfix:after {
	content: '';
	display: table;
	clear: both;
}
.cmsect-container {
	position: relative;
	margin-left: -<?php echo CMSECT_GUTTER_SIZE_PX; ?>px;
	margin-right: -<?php echo CMSECT_GUTTER_SIZE_PX; ?>px
}

*[id^="cmsect"] .loading[type="submit"],
*[class^="cmsect"] .loading[type="submit"] { overflow: hidden; text-indent: -200% }
*[id^="cmsect"] .loading[type="submit"]:after,
*[class^="cmsect"] .loading[type="submit"]:after {
	content: '';
	border-color: rgba(255, 255, 255, 0.2) rgba(255, 255, 255, 0.05) rgba(255, 255, 255, 0.05);
	border-width: 3px;
	left: 50%;
	margin: -15px 0 0 -15px;
	position: absolute;
	top: 50%;
}

.cmsect {
	width: 100%;
	min-height: 300px;
	height: auto;
}
.cmsect.loading:before {
	background-color: rgba(255, 255, 255, 0.7);
	content: '';
	display: block;
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
}

.cmsect-item {
	   -moz-transition: border 0.3s ease 0s, box-shadow 0.3s ease 0s;
	-webkit-transition: border 0.3s ease 0s, box-shadow 0.3s ease 0s;
	     -o-transition: border 0.3s ease 0s, box-shadow 0.3s ease 0s;
	    -ms-transition: border 0.3s ease 0s, box-shadow 0.3s ease 0s;
	        transition: border 0.3s ease 0s, box-shadow 0.3s ease 0s;
	border: 1px solid rgba(0, 0, 0, 0.12);
	box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.1);
	display: block;
	margin: 0 <?php echo CMSECT_GUTTER_SIZE_PX; ?>px <?php echo CMSECT_GUTTER_SIZE_PX * 2; ?>px;
	min-height: 50px;
	opacity: 0;
	overflow: hidden;
	padding: 7px;
	width: 200px;
}
.cmsect-item,
.cmsect-item:active,
.cmsect-item:hover { text-decoration: none }
.cmsect-item:hover {
	border: 1px solid rgba(0, 0, 0, 0.25);
	box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.2);
}
.cmsect.loading .cmsect-item { opacity: 0 }

.cmsect-item [class^="cmsect-item-"] { display: block; line-height: normal }
.cmsect-item .cmsect-item-title { font-size: 16px }
.cmsect-item .cmsect-item-descr,
.cmsect-item .cmsect-terms-by-taxonomy .cmsect-term { font-size: 11px }
.cmsect-item .cmsect-terms-by-taxonomy .cmsect-term { color: #acf; display: list-item; float: left; list-style: outside none none; }
.cmsect-item .cmsect-terms-by-taxonomy .cmsect-term:after { content: ', ' }
.cmsect-item .cmsect-terms-by-taxonomy .cmsect-term:last-child:after { content: '' } 

.cmsect-item .cmsect-item-thumb img {
	   -moz-transition: opacity 0.5s ease 0s;
	-webkit-transition: opacity 0.5s ease 0s;
	     -o-transition: opacity 0.5s ease 0s;
	    -ms-transition: opacity 0.5s ease 0s;
	        transition: opacity 0.5s ease 0s;
	display: block;
	border: 0 none;
	height: auto;
	min-width: 100%;
	max-width: 100%;
	opacity: 1;
	vertical-align: middle;
	z-index: 1
}
.cmsect-item .cmsect-item-thumb.loading *,
.cmsect-item:hover .cmsect-item-thumb.has-flip-thumb img { opacity: 0 !important }

.cmsect-item .cmsect-item-thumb:before {
	   -moz-transition: background-color 0.3s ease 0s;
	-webkit-transition: background-color 0.3s ease 0s;
	     -o-transition: background-color 0.3s ease 0s;
	    -ms-transition: background-color 0.3s ease 0s;
	        transition: background-color 0.3s ease 0s;
}

.cmsect-item .cmsect-item-thumb.loading:before { content: ''; display: block; position: absolute; width: 100%; height: 100%; background-color: rgba(0,0,0,0.1) }
.cmsect-item .cmsect-item-thumb.loading:after { content: ''; display: block; left: 50%; margin: -15px auto auto -15px; position: absolute; top: 50% }

.cmsect-item .cmsect-item-thumb .flip-thumb {
	   -moz-transition: opacity 0.5s ease 0;
	-webkit-transition: opacity 0.5s ease 0;
	     -o-transition: opacity 0.5s ease 0;
	    -ms-transition: opacity 0.5s ease 0;
	        transition: opacity 0.5s ease 0;
	background-position: center center;
	background-size: cover;
	opacity: 0;
	height: 100%;
	left: 0;
	position: absolute;
	top: 0;
	width: 100%;
	z-index: 0
}
.cmsect-item.layed-out .cmsect-item-thumb.has-flip-thumb.flip-ready .flip-thumb { opacity: 1 }

.cmsect.loading-more + .cmsect-loading-more,
.cmsect.end-reached + .cmsect-thats-all {
	background-color: #fafafa;
	border-radius: 5px;
	clear: both;
	color: #aaa;
	height: 50px;
	line-height: 50px;
	margin: <?php echo CMSECT_GUTTER_SIZE_PX; ?>px;
	position: relative;
	text-align: center;
}
.cmsect.loading-more + .cmsect-loading-more:before { content: 'Loading more items...' }
.cmsect.end-reached + .cmsect-thats-all { background-color: #FFF }
.cmsect.end-reached + .cmsect-thats-all:before {
	background-color: #fff;
	content: "That\'s all folks!";
	display: inline-block;
	padding: 0 10px;
	position: relative;
	z-index: 1;
}
.cmsect.end-reached + .cmsect-thats-all::after {
	background-color: rgba(0, 0, 0, 0.1);
	content: "";
	display: block;
	height: 1px;
	position: absolute;
	top: 50%;
	width: 100%;
}

/* Global Custom CSS */
<?php
	if($cms_settings = cmsect_get_settings()) {
		if($cms_settings['custom_css']) {
			echo stripslashes($cms_settings['custom_css'])."\r\n";
		}
	}
?>
/* END: Global Custom CSS */
/* Custom CSS */
<?php
	if($cms_sections = cmsect_get_all()) {
		foreach($cms_sections as $cms_section) {
			if(isset($cms_section->options['custom_css'])) {
				if($cms_section->options['custom_css']) {
					echo stripslashes($cms_section->options['custom_css'])."\r\n";
				}
			}
		}
	}
?>
/* END: Custom CSS */